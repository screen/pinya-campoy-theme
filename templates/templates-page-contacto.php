<?php
/**
* Template Name: Template - Formulario de Contacto
*
* @package pinyacampoy
* @subpackage pinyacampoy-mk01-theme
* @since Mk. 1.0
*/
?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="container" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <section id="post-<?php the_ID(); ?>" class="page-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" role="article" itemscope itemtype="http://schema.org/BlogPosting">
            <div class="row justify-content-center">
                <div class="section-title col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <h1 itemprop="headline"><?php the_title(); ?></h1>
                </div>
                <div class="section-container col-xl-6 col-lg-6 col-md-8 col-sm-12 col-12">
                    <?php the_content(); ?>
                    <form class="page-contact-form-container">
                        <div class="container p-0">
                            <div class="row no-gutters">
                                <div class="contact-form-item col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <input type="text" class="form-control custom-form-control" name="fullname" placeholder="<?php _e('NAME', 'pinyacampoy'); ?>" />
                                    <small class="danger custom-danger d-none error-fullname"></small>
                                </div>
                                <div class="contact-form-item col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <input type="text" class="form-control custom-form-control" name="phone" placeholder="<?php _e('PHONE', 'pinyacampoy'); ?>" />
                                    <small class="danger custom-danger d-none error-fullname"></small>
                                </div>
                                <div class="contact-form-item col-12">
                                    <input type="text" class="form-control custom-form-control" name="email" placeholder="<?php _e('EMAIL', 'pinyacampoy'); ?>" />
                                    <small class="danger custom-danger d-none error-email"></small>
                                </div>
                                <div class="contact-form-item col-12">
                                    <input type="text" class="form-control custom-form-control" name="subject" placeholder="<?php _e('SUBJECT', 'pinyacampoy'); ?>" />
                                    <small class="danger custom-danger d-none error-subject"></small>
                                </div>
                                <div class="contact-form-item col-12">
                                    <textarea name="comments" id="comments" cols="4" class="form-control custom-form-control" placeholder="<?php _e('COMMENTS', 'pinyacampoy'); ?>"></textarea>
                                    <small class="danger custom-danger d-none error-comments"></small>
                                </div>

                                <?php $modal_options = get_option('pcy_modal_settings'); ?>
                                <?php if ($modal_options['modal_link'] != '') { ?>
                                <input type="hidden" name="thanks_link" value="<?php echo $modal_options['modal_link']; ?>" />
                                <?php } ?>
                                <div class="contact-form-submit col-12">
                                    <button type="submit" class="btn btn-md btn-warning btn-submit"><?php _e('Book Now', 'pinyacampoy'); ?></button>
                                </div>
                                <input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response" />
                                <div class="contact-form-loader col-12"></div>
                                <div class="contact-form-response col-12"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
