<?php
/**
* Template Name: Template - Gracias
*
* @package Pinya Campoy
* @subpackage pinyacampoy-mk01-theme
* @since Mk. 1.0
*/
?>
<?php get_header('empty'); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <?php $bg_hero_id = get_post_meta(get_the_ID(), 'sd_main_bg_id', true); ?>
        <?php $bg_hero = wp_get_attachment_image_src($bg_hero_id, 'full', false); ?>
        <section class="thanks-main-container col-12" style="background: url(<?php echo $bg_hero[0]; ?>);">
            <div class="row align-items-start justify-content-between">
                <div class="thanks-image col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12 align-self-end">
                    <?php the_post_thumbnail('full', array('class' => 'img-fluid')); ?>
                </div>
                <div class="thanks-content col-xl-7 col-lg-7 col-md-12 col-sm-12 col-12">
                    <div class="header-logo">
                        <a class="thanks-brand" href="<?php echo home_url('/');?>" title="<?php echo get_bloginfo('name'); ?>">
                            <?php ?> <?php $custom_logo_id = get_theme_mod( 'custom_logo' ); ?>
                            <?php $image = wp_get_attachment_image_src( $custom_logo_id , 'full' ); ?>
                            <?php if (!empty($image)) { ?>
                            <img src="<?php echo $image[0];?>" alt="<?php echo get_bloginfo('name'); ?>" class="img-fluid img-thanks-logo" />
                            <?php } ?>
                        </a>
                    </div>
                    <div class="thanks-main-content">
                        <?php the_content(); ?>
                        <div class="social-thanks-container">
                            <?php $social_options = get_option('pcy_social_settings'); ?>
                            <?php if (isset($social_options['facebook'])) { ?>
                            <?php if ($social_options['facebook'] != '' ) { ?>
                            <a href="<?php echo esc_url($social_options['facebook']);?>" title="<?php _e('Visit our Profile on Facebook', 'pinyacampoy'); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
                            <?php } ?>
                            <?php } ?>

                            <?php if (isset($social_options['twitter'])) { ?>
                            <?php if ($social_options['twitter'] != '') { ?>
                            <a href="<?php echo esc_url($social_options['twitter']);?>" title="<?php _e('Visit our Profile on Twitter', 'pinyacampoy'); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
                            <?php } ?>
                            <?php } ?>

                            <?php if (isset($social_options['instagram'])) { ?>
                            <?php if ($social_options['instagram'] != '') { ?>
                            <a href="<?php echo esc_url($social_options['instagram']);?>" title="<?php _e('Visit our Profile on Instagram', 'pinyacampoy'); ?>" target="_blank"><i class="fa fa-instagram"></i></a>
                            <?php } ?>
                            <?php } ?>

                            <?php if (isset($social_options['youtube'])) { ?>
                            <?php if ($social_options['youtube'] != '') { ?>
                            <a href="<?php echo esc_url($social_options['youtube']);?>" title="<?php _e('Visit our channel on YouTube', 'pinyacampoy'); ?>" target="_blank"><i class="fa fa-youtube-play"></i></a>
                            <?php } ?>
                            <?php } ?>

                            <?php if (isset($social_options['linkedin'])) { ?>
                            <?php if ($social_options['linkedin'] != '') { ?>
                            <a href="<?php echo esc_url($social_options['linkedin']);?>" title="<?php _e('Visit our Profile on LinkedIn', 'pinyacampoy'); ?>" target="_blank"><i class="fa fa-linkedin"></i></a>
                            <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>
</main>
<?php get_footer('empty'); ?>
