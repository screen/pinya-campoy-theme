<!DOCTYPE html>
<html <?php language_attributes() ?>>

<head>
    <?php /* MAIN STUFF */ ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo('charset') ?>" />
    <meta name="robots" content="NOODP, INDEX, FOLLOW" />
    <meta name="HandheldFriendly" content="True" />
    <meta name="MobileOptimized" content="320" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="pingback" href="<?php echo esc_url(get_bloginfo('pingback_url')); ?>" />
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="dns-prefetch" href="//connect.facebook.net" />
    <link rel="dns-prefetch" href="//facebook.com" />
    <link rel="dns-prefetch" href="//googleads.g.doubleclick.net" />
    <link rel="dns-prefetch" href="//pagead2.googlesyndication.com" />
    <link rel="dns-prefetch" href="//google-analytics.com" />
    <?php /* FAVICONS */ ?>
    <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon.png" />
    <?php /* THEME NAVBAR COLOR */ ?>
    <meta name="msapplication-TileColor" content="#C5B683" />
    <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/images/win8-tile-icon.png" />
    <meta name="theme-color" content="#C5B683" />
    <?php /* AUTHOR INFORMATION */ ?>
    <meta name="language" content="<?php echo get_bloginfo('language'); ?>" />
    <meta name="author" content="Pinya Campoy" />
    <meta name="copyright" content="https://pinyacampoy.com" />
    <meta name="geo.region" content="US" />
    <meta name="DC.title" content="<?php if (is_home()) { echo get_bloginfo('name') . ' | ' . get_bloginfo('description'); } else { echo get_the_title() . ' | ' . get_bloginfo('name'); } ?>" />
    <?php /* MAIN TITLE - CALL HEADER MAIN FUNCTIONS */ ?>
    <?php wp_title('|', false, 'right'); ?>
    <?php wp_head() ?>
    <?php /* OPEN GRAPHS INFO - COMMENTS SCRIPTS */ ?>
    <?php if ( is_single('post') && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
    <?php /* IE COMPATIBILITIES */ ?>
    <!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7" /><![endif]-->
    <!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8" /><![endif]-->
    <!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9" /><![endif]-->
    <!--[if gt IE 8]><!-->
    <html <?php language_attributes(); ?> class="no-js" />
    <!--<![endif]-->
    <!--[if IE]> <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script> <![endif]-->
    <!--[if IE]> <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script> <![endif]-->
    <!--[if IE]> <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" /> <![endif]-->
</head>

<body class="the-main-body <?php echo join(' ', get_body_class()); ?>" itemscope itemtype="http://schema.org/WebPage">
    <?php wp_body_open(); ?>
    <div id="fb-root"></div>
    <header id="sticker" class="container-fluid container-header p-0" role="banner" itemscope itemtype="http://schema.org/WPHeader">
        <div class="row no-gutters">
            <div class="the-header col-xl-10 offset-xl-2 col-lg-11 offset-lg-1 col-md-12 col-sm-12 col-12">
                <div class="container-fluid d-xl-block d-lg-block d-md-none d-sm-none d-none">
                    <div class="row no-gutters align-items-center">
                        <div class="header-left col-xl-8 col-lg-8 col-md-7 col-sm-7 col-6">
                            <nav class="navbar navbar-expand-md navbar-light" role="navigation">
                                <a class="navbar-brand" href="<?php echo home_url('/');?>" title="<?php echo get_bloginfo('name'); ?>">
                                    <?php $custom_logo_id = get_theme_mod( 'custom_logo' ); ?>
                                    <?php $image = wp_get_attachment_image_src( $custom_logo_id , 'logo' ); ?>
                                    <?php if (!empty($image)) { ?>
                                    <img src="<?php echo $image[0];?>" alt="<?php echo get_bloginfo('name'); ?>" class="img-fluid img-logo" />
                                    <?php } else { ?>
                                    Navbar
                                    <?php } ?>
                                </a>
                                <!-- Brand and toggle get grouped for better mobile display -->
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-controls="bs-example-navbar-collapse-1" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                                <?php
                                    wp_nav_menu( array(
                                        'theme_location'  => 'header_menu',
                                        'depth'           => 2, // 1 = no dropdowns, 2 = with dropdowns.
                                        'container'       => 'div',
                                        'container_class' => 'collapse navbar-collapse',
                                        'container_id'    => 'bs-example-navbar-collapse-1',
                                        'menu_class'      => 'navbar-nav ml-auto mr-auto',
                                        'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
                                        'walker'          => new WP_Bootstrap_Navwalker(),
                                    ) );
                                    ?>

                            </nav>
                        </div>
                        <div class="header-right col-xl-4 col-lg-4 col-md-5 col-sm-5 col-6">
                            <?php if ((is_page('contactanos')) || (is_page('contact-us'))) { $class = 'btn-header-active'; $modal = ''; } else { $class = ''; $modal = 'data-toggle="modal" data-target="#ModalAppointment"'; } ?>
                            <button class="btn btn-md btn-header <?php echo $class; ?>" title="<?php _e('Click here to Book an Appointment', 'pinyacampoy'); ?>" <?php echo $modal; ?>><?php _e('Book Appointment', 'pinyacampoy'); ?></button>
                            <div class="input-group btn btn-lang">
                                <div class="input-group-prepend">
                                    <button class="btn btn-outline-secondary dropdown-toggle btn-lang-inner" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php if (get_locale() == 'en_US') { ?>
                                        EN
                                        <?php } else { ?>
                                        ES
                                        <?php } ?></button>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" title="<?php _e('Ir a Sitio en Español', 'pinyacampoy'); ?>" href="<?php echo network_home_url('/es'); ?>">ES</a>
                                        <a class="dropdown-item" title="<?php _e('Go to site in English', 'pinyacampoy'); ?>" href="<?php echo network_home_url('/'); ?>">EN</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid mobile-container d-xl-none d-lg-none d-md-block d-sm-block d-block">
                    <div class="row no-gutters align-items-center">
                        <div class="navbar-brand-container col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                            <a class="navbar-brand" href="<?php echo home_url('/');?>" title="<?php echo get_bloginfo('name'); ?>">
                                <?php $custom_logo_id = get_theme_mod( 'custom_logo' ); ?>
                                <?php $image = wp_get_attachment_image_src( $custom_logo_id , 'logo' ); ?>
                                <?php if (!empty($image)) { ?>
                                <img src="<?php echo $image[0];?>" alt="<?php echo get_bloginfo('name'); ?>" class="img-fluid img-logo" />
                                <?php } else { ?>
                                Navbar
                                <?php } ?>
                            </a>
                        </div>
                        <div class="navbar-button-container col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                            <div class="input-group btn btn-lang">
                                <div class="input-group-prepend">
                                    <button class="btn btn-outline-secondary dropdown-toggle btn-lang-inner" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php if (get_locale() == 'en_US') { ?>
                                        EN
                                        <?php } else { ?>
                                        ES
                                        <?php } ?></button>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" title="<?php _e('Ir a Sitio en Español', 'pinyacampoy'); ?>" href="<?php echo network_home_url('/es'); ?>">ES</a>
                                        <a class="dropdown-item" title="<?php _e('Go to site in English', 'pinyacampoy'); ?>" href="<?php echo network_home_url('/'); ?>">EN</a>
                                    </div>
                                </div>
                            </div>
                            <button class="navbar-button">
                                <span></span>
                                <span></span>
                                <span></span>
                            </button>
                        </div>
                        <div class="navbar-mobile-container navbar-mobile-container-hidden col-12">
                            <?php
                                wp_nav_menu( array(
                                    'theme_location'  => 'header_menu',
                                    'depth'           => 2, // 1 = no dropdowns, 2 = with dropdowns.
                                    'container'       => 'div'
                                ) );
                                ?>
                            <?php if ((is_page('contactanos')) || (is_page('contact-us'))) { $class = 'btn-header-active'; } else { $class = ''; } ?>
                            <button class="btn btn-md btn-header <?php echo $class; ?>" title="<?php _e('Click here to Book an Appointment', 'pinyacampoy'); ?>" data-toggle="modal" data-target="#ModalAppointment"><?php _e('Book Appointment', 'pinyacampoy'); ?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
