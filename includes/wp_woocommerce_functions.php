<?php
/* WOOCOMMERCE CUSTOM COMMANDS */

/* WOOCOMMERCE - DECLARE THEME SUPPORT - BEGIN */
add_action( 'after_setup_theme', 'pinyacampoy_woocommerce_support' );
function pinyacampoy_woocommerce_support() {
    add_theme_support( 'woocommerce' );
    add_theme_support( 'wc-product-gallery-zoom' );
    add_theme_support( 'wc-product-gallery-lightbox' );
    add_theme_support( 'wc-product-gallery-slider' );
}
add_filter( 'woocommerce_get_image_size_gallery_thumbnail', function( $size ) {
    return array(
        'width'  => 190,
        'height' => 190,
        'crop'   => 0,
    );
} );
/* WOOCOMMERCE - DECLARE THEME SUPPORT - END */

/* WOOCOMMERCE - CUSTOM WRAPPER - BEGIN */
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

add_action('woocommerce_before_main_content', 'pinyacampoy_woocommerce_custom_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'pinyacampoy_woocommerce_custom_wrapper_end', 10);

function pinyacampoy_woocommerce_custom_wrapper_start() {
    echo '<section id="main" class="container-fluid"><div class="row justify-content-center"><div class="woocustom-main-container col-xl-10 col-lg-10 col-md-10 col-sm-12 col-12">';
}

function pinyacampoy_woocommerce_custom_wrapper_end() {
    echo '</div></div></section>';
}
/* WOOCOMMERCE - CUSTOM WRAPPER - END */

/* WOOCOMMERCE - ARCHIVE PRODUCT - START */
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
/* WOOCOMMERCE - ARCHIVE PRODUCT - END */


/* WOOCOMMERCE - CUSTOM CONTENT PRODUCT - SHOP - START */
// Override theme default specification for product # per row
add_filter('loop_shop_columns', 'woocommerce_custom_loop_columns', 999);

function woocommerce_custom_loop_columns() {
    return 3;
}

add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args' );
function jk_related_products_args( $args ) {
    $args['posts_per_page'] = 3; // 4 related products
    $args['columns'] = 3; // arranged in 2 columns
    return $args;
}
//remove_action('woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10);
//remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5);
remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);
remove_action('woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10);
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);
remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);
remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);
/* WOOCOMMERCE - CUSTOM CONTENT PRODUCT - SHOP - END */


/* WOOCOMMERCE - SINGLE PRODUCT - START */
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
add_action('woocommerce_single_product_summary', 'custom_woocommerce_breadcrumb', 4);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
add_action('woocommerce_single_product_summary', 'custom_woocommerce_template_single_excerpt', 20);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50);
add_action('woocommerce_single_product_summary', 'custom_woocommerce_template_single_sharing', 50);
/* WOOCOMMERCE - SINGLE PRODUCT - END */

function custom_woocommerce_template_single_sharing() {
    if (is_single()) {
        ob_start();
?>
<div class="custom-social-share">
    <?php $custom_text = __('This company makes amazing custom handmade shoes. Look at these ones!', 'pinyacampoy'); ?>
    <span><?php _e('Share', 'pinyacampoy'); ?>:</span>
    <div class="custom-social-share-content">
        <a href="https://www.facebook.com/sharer.php?u=<?php echo get_permalink(); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
        <a href="https://twitter.com/share?url=<?php echo get_permalink(); ?>&text=<?php echo $custom_text . ' ' . get_the_title(); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
        <a href="https://api.whatsapp.com/send?text=<?php echo $custom_text . ' ' . get_the_title(); ?> <?php echo get_permalink(); ?>" target="_blank"><i class="fa fa-whatsapp"></i></a>
        <a href="https://pinterest.com/pin/create/bookmarklet/?media=<?php echo get_the_post_thumbnail_url(); ?>&url=<?php echo get_permalink(); ?>&description=<?php echo $custom_text . ' ' . get_the_title(); ?>" target="_blank"><i class="fa fa-pinterest"></i></a>
    </div>
</div>
<?php
        $content = ob_get_clean();
        echo $content;
    }
}

function custom_woocommerce_template_single_excerpt() {
?>
<div class="custom-single-product-content">
    <?php echo get_the_content(); ?>
</div>
<?php
                                                      }

function custom_woocommerce_breadcrumb() {
    ob_start();
    $shop_page_url = get_permalink( woocommerce_get_page_id( 'shop' ) );
    $category_array = get_the_terms(get_the_ID(), 'product_cat');
    foreach ($category_array as $category_item) {
        if ($category_item->parent == 0) {
            $id_explode = $category_item->term_id;
            $name_explode = $category_item->name;
        }
    }

?>
<div class="custom-breadcrumb-container">
    <ul>
        <li><a href="<?php echo home_url('/'); ?>"><?php _e('Home', 'pinyacampoy'); ?></a></li>
        <li><a href="<?php echo $shop_page_url; ?>"><?php _e('Catalog', 'pinyacampoy'); ?></a></li>
        <li><a href="<?php echo get_term_link($id_explode, 'product_cat'); ?>"><?php echo $name_explode; ?></a></li>
        <li><span><?php echo get_the_title(); ?></span></li>
    </ul>
</div>
<?php
    $content = ob_get_clean();
    echo $content;
}
