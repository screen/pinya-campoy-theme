<?php

function pinyacampoy_custom_post_type() {

	$labels = array(
		'name'                  => _x( 'Clientes', 'Post Type General Name', 'pinyacampoy' ),
		'singular_name'         => _x( 'Cliente', 'Post Type Singular Name', 'pinyacampoy' ),
		'menu_name'             => __( 'Clientes', 'pinyacampoy' ),
		'name_admin_bar'        => __( 'Clientes', 'pinyacampoy' ),
		'archives'              => __( 'Archivo de Clientes', 'pinyacampoy' ),
		'attributes'            => __( 'Atributos de Cliente', 'pinyacampoy' ),
		'parent_item_colon'     => __( 'Cliente Padre:', 'pinyacampoy' ),
		'all_items'             => __( 'Todos los Clientes', 'pinyacampoy' ),
		'add_new_item'          => __( 'Agregar Nuevo Cliente', 'pinyacampoy' ),
		'add_new'               => __( 'Agregar Nuevo', 'pinyacampoy' ),
		'new_item'              => __( 'Nuevo Cliente', 'pinyacampoy' ),
		'edit_item'             => __( 'Editar Cliente', 'pinyacampoy' ),
		'update_item'           => __( 'Actualizar Cliente', 'pinyacampoy' ),
		'view_item'             => __( 'Ver Cliente', 'pinyacampoy' ),
		'view_items'            => __( 'Ver Clientes', 'pinyacampoy' ),
		'search_items'          => __( 'Buscar Cliente', 'pinyacampoy' ),
		'not_found'             => __( 'No hay resultados', 'pinyacampoy' ),
		'not_found_in_trash'    => __( 'No hay resultados en Papelera', 'pinyacampoy' ),
		'featured_image'        => __( 'Imagen del Cliente', 'pinyacampoy' ),
		'set_featured_image'    => __( 'Colocar Imagen del Cliente', 'pinyacampoy' ),
		'remove_featured_image' => __( 'Remover Imagen del Cliente', 'pinyacampoy' ),
		'use_featured_image'    => __( 'Usar como Imagen del Cliente', 'pinyacampoy' ),
		'insert_into_item'      => __( 'Insertar en Cliente', 'pinyacampoy' ),
		'uploaded_to_this_item' => __( 'Cargado a este Cliente', 'pinyacampoy' ),
		'items_list'            => __( 'Listado de Clientes', 'pinyacampoy' ),
		'items_list_navigation' => __( 'Navegación del Listado de Cliente', 'pinyacampoy' ),
		'filter_items_list'     => __( 'Filtro del Listado de Clientes', 'pinyacampoy' ),
	);
	$args = array(
		'label'                 => __( 'Cliente', 'pinyacampoy' ),
		'description'           => __( 'Portafolio de Clientes', 'pinyacampoy' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'            => array( 'tipos-de-clientes' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 15,
		'menu_icon'             => 'dashicons-businessperson',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
	);
	register_post_type( 'clientes', $args );

}

add_action( 'init', 'pinyacampoy_custom_post_type', 0 );