<?php
/* --------------------------------------------------------------
WP CUSTOMIZE SECTION - CUSTOM SETTINGS
-------------------------------------------------------------- */

add_action( 'customize_register', 'pinyacampoy_customize_register' );

function pinyacampoy_customize_register( $wp_customize ) {

    /* HEADER */
    $wp_customize->add_section('pcy_header_settings', array(
        'title'    => __('Header', 'pinyacampoy'),
        'description' => __('Options for Header Elements', 'pinyacampoy'),
        'priority' => 30
    ));

    $wp_customize->add_setting('pcy_header_settings[phone_number]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'
    ));

    $wp_customize->add_control( 'phone_number', array(
        'type' => 'text',
        'label'    => __('Phone Number', 'pinyacampoy'),
        'description' => __( 'Add phone number link for Whatsapp Link', 'pinyacampoy' ),
        'section'  => 'pcy_header_settings',
        'settings' => 'pcy_header_settings[phone_number]'
    ));

    $wp_customize->add_setting('pcy_header_settings[email_address]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'

    ));

    $wp_customize->add_control( 'email_address', array(
        'type' => 'text',
        'label'    => __('Email Address', 'pinyacampoy'),
        'description' => __( 'Add Email address for forms and CTA', 'pinyacampoy' ),
        'section'  => 'pcy_header_settings',
        'settings' => 'pcy_header_settings[email_address]'
    ));

    /* SOCIAL */
    $wp_customize->add_section('pcy_social_settings', array(
        'title'    => __('Redes Sociales', 'pinyacampoy'),
        'description' => __('Agregue aqui las redes sociales de la página, serán usadas globalmente', 'pinyacampoy'),
        'priority' => 175,
    ));

    $wp_customize->add_setting('pcy_social_settings[facebook]', array(
        'default'           => '',
        'sanitize_callback' => 'pinyacampoy_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'facebook', array(
        'type' => 'url',
        'section' => 'pcy_social_settings',
        'settings' => 'pcy_social_settings[facebook]',
        'label' => __( 'Facebook', 'pinyacampoy' ),
    ) );

    $wp_customize->add_setting('pcy_social_settings[twitter]', array(
        'default'           => '',
        'sanitize_callback' => 'pinyacampoy_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'twitter', array(
        'type' => 'url',
        'section' => 'pcy_social_settings',
        'settings' => 'pcy_social_settings[twitter]',
        'label' => __( 'Twitter', 'pinyacampoy' ),
    ) );

    $wp_customize->add_setting('pcy_social_settings[instagram]', array(
        'default'           => '',
        'sanitize_callback' => 'pinyacampoy_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'instagram', array(
        'type' => 'url',
        'section' => 'pcy_social_settings',
        'settings' => 'pcy_social_settings[instagram]',
        'label' => __( 'Instagram', 'pinyacampoy' ),
    ) );

    $wp_customize->add_setting('pcy_social_settings[linkedin]', array(
        'default'           => '',
        'sanitize_callback' => 'pinyacampoy_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'linkedin', array(
        'type' => 'url',
        'section' => 'pcy_social_settings',
        'settings' => 'pcy_social_settings[linkedin]',
        'label' => __( 'LinkedIn', 'pinyacampoy' ),
    ) );

    $wp_customize->add_setting('pcy_social_settings[youtube]', array(
        'default'           => '',
        'sanitize_callback' => 'pinyacampoy_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'youtube', array(
        'type' => 'url',
        'section' => 'pcy_social_settings',
        'settings' => 'pcy_social_settings[youtube]',
        'label' => __( 'YouTube', 'pinyacampoy' ),
    ) );

    /* MODAL */
    $wp_customize->add_section('pcy_modal_settings', array(
        'title'    => __('Modal', 'pinyacampoy'),
        'description' => __('Opciones de Modal', 'pinyacampoy'),
        'priority' => 176,
    ));

    $wp_customize->add_setting('pcy_modal_settings[modal_title]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'

    ));

    $wp_customize->add_control( 'modal_title', array(
        'type' => 'text',
        'label'    => __('Título', 'pinyacampoy'),
        'description' => __( 'Título descriptivo del Modal.' ),
        'section'  => 'pcy_modal_settings',
        'settings' => 'pcy_modal_settings[modal_title]'
    ));

    $wp_customize->add_setting('pcy_modal_settings[modal_text]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'

    ));

    $wp_customize->add_control( 'modal_text', array(
        'type' => 'textarea',
        'label'    => __('Descripción', 'pinyacampoy'),
        'description' => __( 'Texto descriptivo del Modal.' ),
        'section'  => 'pcy_modal_settings',
        'settings' => 'pcy_modal_settings[modal_text]'
    ));

    $wp_customize->add_setting('pcy_modal_settings[modal_link]', array(
        'default'           => '',
        'sanitize_callback' => 'absint',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',
    ));

    $wp_customize->add_control( 'modal_link', array(
        'type'     => 'dropdown-pages',
        'label' => __( 'Link Pagina de Agradecimiento', 'pinyacampoy' ),
        'section' => 'pcy_modal_settings',
        'settings' => 'pcy_modal_settings[modal_link]',
    ) );

    /* COOKIES */
    $wp_customize->add_section('pcy_cookie_settings', array(
        'title'    => __('Cookies', 'pinyacampoy'),
        'description' => __('Opciones de Cookies', 'pinyacampoy'),
        'priority' => 176,
    ));

    $wp_customize->add_setting('pcy_cookie_settings[cookie_text]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'

    ));

    $wp_customize->add_control( 'cookie_text', array(
        'type' => 'textarea',
        'label'    => __('Cookie consent', 'pinyacampoy'),
        'description' => __( 'Texto del Cookie consent.' ),
        'section'  => 'pcy_cookie_settings',
        'settings' => 'pcy_cookie_settings[cookie_text]'
    ));

    $wp_customize->add_setting('pcy_cookie_settings[cookie_link]', array(
        'default'           => '',
        'sanitize_callback' => 'absint',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'cookie_link', array(
        'type'     => 'dropdown-pages',
        'section' => 'pcy_cookie_settings',
        'settings' => 'pcy_cookie_settings[cookie_link]',
        'label' => __( 'Link de Cookies', 'pinyacampoy' ),
    ) );

}

function pinyacampoy_sanitize_url( $url ) {
    return esc_url_raw( $url );
}

/* --------------------------------------------------------------
CUSTOM CONTROL PANEL
-------------------------------------------------------------- */
/*
function register_pinyacampoy_settings() {
    register_setting( 'pinyacampoy-settings-group', 'monday_start' );
    register_setting( 'pinyacampoy-settings-group', 'monday_end' );
    register_setting( 'pinyacampoy-settings-group', 'monday_all' );
}

add_action('admin_menu', 'pinyacampoy_custom_panel_control');

function pinyacampoy_custom_panel_control() {
    add_menu_page(
        __( 'Panel de Control', 'pinyacampoy' ),
        __( 'Panel de Control','pinyacampoy' ),
        'manage_options',
        'pinyacampoy-control-panel',
        'pinyacampoy_control_panel_callback',
        'dashicons-admin-generic',
        120
    );
    add_action( 'admin_init', 'register_pinyacampoy_settings' );
}

function pinyacampoy_control_panel_callback() {
    ob_start();
?>
<div class="pinyacampoy-admin-header-container">
    <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="pinyacampoy" />
    <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
</div>
<form method="post" action="options.php" class="pinyacampoy-admin-content-container">
    <?php settings_fields( 'pinyacampoy-settings-group' ); ?>
    <?php do_settings_sections( 'pinyacampoy-settings-group' ); ?>
    <div class="pinyacampoy-admin-content-item">
        <table class="form-table">
            <tr valign="center">
                <th scope="row"><?php _e('Monday', 'pinyacampoy'); ?></th>
                <td>
                    <label for="monday_start">Starting Hour: <input type="time" name="monday_start" value="<?php echo esc_attr( get_option('monday_start') ); ?>"></label>
                    <label for="monday_end">Ending Hour: <input type="time" name="monday_end" value="<?php echo esc_attr( get_option('monday_end') ); ?>"></label>
                    <label for="monday_all">All Day: <input type="checkbox" name="monday_all" value="1" <?php checked( get_option('monday_all'), 1 ); ?>></label>
                </td>
            </tr>
        </table>
    </div>
    <div class="pinyacampoy-admin-content-submit">
        <?php submit_button(); ?>
    </div>
</form>
<?php
    $content = ob_get_clean();
    echo $content;
}
*/
