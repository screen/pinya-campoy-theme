<?php
function ed_metabox_include_front_page( $display, $meta_box ) {
    if ( ! isset( $meta_box['show_on']['key'] ) ) {
        return $display;
    }

    if ( 'front-page' !== $meta_box['show_on']['key'] ) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if ( isset( $_GET['post'] ) ) {
        $post_id = $_GET['post'];
    } elseif ( isset( $_POST['post_ID'] ) ) {
        $post_id = $_POST['post_ID'];
    }

    if ( ! $post_id ) {
        return false;
    }

    // Get ID of page set as front page, 0 if there isn't one
    $front_page = get_option( 'page_on_front' );

    // there is a front page set and we're on it!
    return $post_id == $front_page;
}
add_filter( 'cmb2_show_on', 'ed_metabox_include_front_page', 10, 2 );

function be_metabox_show_on_slug( $display, $meta_box ) {
    if ( ! isset( $meta_box['show_on']['key'], $meta_box['show_on']['value'] ) ) {
        return $display;
    }

    if ( 'slug' !== $meta_box['show_on']['key'] ) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if ( isset( $_GET['post'] ) ) {
        $post_id = $_GET['post'];
    } elseif ( isset( $_POST['post_ID'] ) ) {
        $post_id = $_POST['post_ID'];
    }

    if ( ! $post_id ) {
        return $display;
    }

    $slug = get_post( $post_id )->post_name;

    // See if there's a match
    return in_array( $slug, (array) $meta_box['show_on']['value']);
}
add_filter( 'cmb2_show_on', 'be_metabox_show_on_slug', 10, 2 );

add_action( 'cmb2_admin_init', 'pinyacampoy_register_custom_metabox' );
function pinyacampoy_register_custom_metabox() {
    $prefix = 'pcy_';

    /* --------------------------------------------------------------
        1.- MAIN SLIDER
    -------------------------------------------------------------- */
    $cmb_home_slider = new_cmb2_box( array(
        'id'            => $prefix . 'home_slider_metabox',
        'title'         => esc_html__( 'Home: Slider Section', 'pinyacampoy' ),
        'object_types'  => array( 'page' ), // Post type
        'show_on'       => array( 'key' => 'front-page', 'value' => '' ),
        'closed'        => true
    ) );

    $cmb_home_slider->add_field( array(
        'name'       => esc_html__( 'Activate Section', 'pinyacampoy' ),
        'desc'       => esc_html__( 'Check if you want to activate this section', 'pinyacampoy' ),
        'id'         => $prefix . 'activate_slider',
        'type'       => 'checkbox',
    ) );



    $group_field_id = $cmb_home_slider->add_field( array(
        'id'          => $prefix . 'home_slider_group',
        'type'        => 'group',
        'description' => __( 'Slides inside this Slider', 'pinyacampoy' ),
        'options'     => array(
            'group_title'       => __( 'Slide {#}', 'pinyacampoy' ),
            'add_button'        => __( 'Add other Slide', 'pinyacampoy' ),
            'remove_button'     => __( 'Remove Slide', 'pinyacampoy' ),
            'sortable'          => true,
            'closed'         => true,
            'remove_confirm' => esc_html__( 'Are you sure to remove this slide?', 'pinyacampoy' )
        )
    ) );

    $cmb_home_slider->add_group_field( $group_field_id, array(
        'id'   => 'bg_image',
        'name'      => esc_html__( 'Slider Background Image', 'pinyacampoy' ),
        'desc'      => esc_html__( 'Upload a background for this slide', 'pinyacampoy' ),
        'type'    => 'file',

        'options' => array(
            'url' => false
        ),
        'text'    => array(
            'add_upload_file_text' => esc_html__( 'Upload Background', 'pinyacampoy' ),
        ),
        'query_args' => array(
            'type' => array(
                'image/gif',
                'image/jpeg',
                'image/png'
            )
        ),
        'preview_size' => 'medium'
    ) );

    $cmb_home_slider->add_group_field( $group_field_id, array(
        'id'   => 'title',
        'name'      => esc_html__( 'Slider Title', 'pinyacampoy' ),
        'desc'      => esc_html__( 'Enter a descriptive title for all slides', 'pinyacampoy' ),
        'type' => 'text'
    ) );

    $cmb_home_slider->add_group_field( $group_field_id, array(
        'id'   => 'description',
        'name'      => esc_html__( 'Slider Text', 'pinyacampoy' ),
        'desc'      => esc_html__( 'Enter a descriptive text for all slides', 'pinyacampoy' ),
        'type' => 'textarea_small'
    ) );

    $cmb_home_slider->add_group_field( $group_field_id, array(
        'id'         => 'modal',
        'name'       => esc_html__( 'Activate Video Modal on Button', 'pinyacampoy' ),
        'desc'       => esc_html__( 'Check if you want to activate the video modal functions on this button', 'pinyacampoy' ),
        'type'       => 'checkbox',
    ) );

    $cmb_home_slider->add_group_field( $group_field_id, array(
        'id'   => 'button_bgcolor',
        'name'      => esc_html__( 'Button BG Color', 'pinyacampoy' ),
        'desc'      => esc_html__( 'Select an HEX color for this button', 'pinyacampoy' ),
        'type'    => 'colorpicker',
        'default' => '#C70034',
    ) );

    $cmb_home_slider->add_group_field( $group_field_id, array(
        'id'   => 'button_txcolor',
        'name'      => esc_html__( 'Button Text Color', 'pinyacampoy' ),
        'desc'      => esc_html__( 'Select an HEX color for this button', 'pinyacampoy' ),
        'type'    => 'colorpicker',
        'default' => '#C70034',
    ) );

    $cmb_home_slider->add_group_field( $group_field_id, array(
        'id'   => 'button_title',
        'name'      => esc_html__( 'Button Title', 'pinyacampoy' ),
        'desc'      => esc_html__( 'Enter a descriptive title for this button', 'pinyacampoy' ),
        'type' => 'text'
    ) );

    $cmb_home_slider->add_group_field( $group_field_id, array(
        'id'   => 'button_url',
        'name'      => esc_html__( 'Button URL', 'pinyacampoy' ),
        'desc'      => esc_html__( 'Enter a URL for this button (If video modal is not selected)', 'pinyacampoy' ),
        'type' => 'text_url'
    ) );

    $cmb_home_slider->add_group_field( $group_field_id, array(
        'id'   => 'video_mp4',
        'name'      => esc_html__( 'Video: Mp4', 'pinyacampoy' ),
        'desc'      => esc_html__( 'Upload a .mp4 video', 'pinyacampoy' ),
        'type'    => 'file',

        'options' => array(
            'url' => false
        ),
        'text'    => array(
            'add_upload_file_text' => esc_html__( 'Upload video', 'pinyacampoy' ),
        ),
        'query_args' => array(
            'type' => array(
                'video/mp4'
            )
        ),
        'preview_size' => 'medium'
    ) );

    $cmb_home_slider->add_group_field( $group_field_id, array(
        'id'   => 'video_webm',
        'name'      => esc_html__( 'Video: WebM', 'pinyacampoy' ),
        'desc'      => esc_html__( 'Upload a .webm video', 'pinyacampoy' ),
        'type'    => 'file',

        'options' => array(
            'url' => false
        ),
        'text'    => array(
            'add_upload_file_text' => esc_html__( 'Upload video', 'pinyacampoy' ),
        ),
        'query_args' => array(
            'type' => array(
                'video/webm'
            )
        ),
        'preview_size' => 'medium'
    ) );

    $cmb_home_slider->add_group_field( $group_field_id, array(
        'id'   => 'video_ogv',
        'name'      => esc_html__( 'Video: OGV', 'pinyacampoy' ),
        'desc'      => esc_html__( 'Upload a .ogv video', 'pinyacampoy' ),
        'type'    => 'file',

        'options' => array(
            'url' => false
        ),
        'text'    => array(
            'add_upload_file_text' => esc_html__( 'Upload video', 'pinyacampoy' ),
        ),
        'query_args' => array(
            'type' => array(
                'video/ogg'
            )
        ),
        'preview_size' => 'medium'
    ) );

    $cmb_home_slider->add_group_field( $group_field_id, array(
        'id'   => 'poster',
        'name'      => esc_html__( 'Video Poster Image', 'pinyacampoy' ),
        'desc'      => esc_html__( 'Upload a image postser for this video', 'pinyacampoy' ),
        'type'    => 'file',

        'options' => array(
            'url' => false
        ),
        'text'    => array(
            'add_upload_file_text' => esc_html__( 'Upload image', 'pinyacampoy' ),
        ),
        'query_args' => array(
            'type' => array(
                'image/gif',
                'image/jpeg',
                'image/png'
            )
        ),
        'preview_size' => 'medium'
    ) );

    /* --------------------------------------------------------------
        2.- ABOUT SECTION
    -------------------------------------------------------------- */
    $cmb_home_about = new_cmb2_box( array(
        'id'            => $prefix . 'home_about_metabox',
        'title'         => esc_html__( 'Home: About Section', 'pinyacampoy' ),
        'object_types'  => array( 'page' ), // Post type
        'show_on'       => array( 'key' => 'front-page', 'value' => '' ),
        'closed'        => true
    ) );

    $cmb_home_about->add_field( array(
        'name'       => esc_html__( 'Activate Section', 'pinyacampoy' ),
        'desc'       => esc_html__( 'Check if you want to activate this section', 'pinyacampoy' ),
        'id'         => $prefix . 'activate_about',
        'type'       => 'checkbox',
    ) );

    $group_field_id = $cmb_home_about->add_field( array(
        'id'          => $prefix . 'home_about_group',
        'type'        => 'group',
        'title'         => esc_html__( 'About Slider', 'pinyacampoy' ),
        'description' => __( 'Slides inside this Slider', 'pinyacampoy' ),
        'options'     => array(
            'group_title'       => __( 'Slide {#}', 'pinyacampoy' ),
            'add_button'        => __( 'Add other Slide', 'pinyacampoy' ),
            'remove_button'     => __( 'Remove Slide', 'pinyacampoy' ),
            'sortable'          => true,
            'closed'         => true,
            'remove_confirm' => esc_html__( 'Are you sure to remove this slide?', 'pinyacampoy' )
        )
    ) );

    $cmb_home_about->add_group_field( $group_field_id, array(
        'id'   => 'title',
        'name'      => esc_html__( 'Slider Title', 'pinyacampoy' ),
        'desc'      => esc_html__( 'Enter a descriptive title for all slides', 'pinyacampoy' ),
        'type' => 'text'
    ) );

    $cmb_home_about->add_group_field( $group_field_id, array(
        'id'   => 'subtitle',
        'name'      => esc_html__( 'Slider Subtitle', 'pinyacampoy' ),
        'desc'      => esc_html__( 'Enter a descriptive Subtitle', 'pinyacampoy' ),
        'type' => 'text'
    ) );

    $cmb_home_about->add_group_field( $group_field_id, array(
        'id'   => 'description',
        'name'      => esc_html__( 'Slider Text', 'pinyacampoy' ),
        'desc'      => esc_html__( 'Enter a descriptive text for all slides', 'pinyacampoy' ),
        'type'    => 'wysiwyg',
        'options' => array(
            'textarea_rows' => get_option('default_post_edit_rows', 4),
            'teeny' => false
        )
    ) );

    $cmb_home_about->add_group_field( $group_field_id, array(
        'id'   => 'logo_list',
        'name'      => esc_html__( 'Logo List', 'pinyacampoy' ),
        'desc'      => esc_html__( 'Upload a Logo List if neccesary here', 'pinyacampoy' ),
        'type' => 'file_list',
        'preview_size' => array( 100, 100 ),
        'query_args' => array( 'type' => 'image' ),
        'text' => array(
            'add_upload_files_text' => esc_html__( 'Upload Logo', 'pinyacampoy' ),
            'remove_image_text' => esc_html__( 'Remove Logo', 'pinyacampoy' ),
            'file_text' => esc_html__( 'Logo', 'pinyacampoy' ),
            'file_download_text' => esc_html__( 'Download', 'pinyacampoy' ),
            'remove_text' => esc_html__( 'Remove', 'pinyacampoy' ),
        )
    ) );

    $cmb_home_about->add_group_field( $group_field_id, array(
        'id'   => 'bg_image',
        'name'      => esc_html__( 'Slider Image', 'pinyacampoy' ),
        'desc'      => esc_html__( 'Upload a background for this slide', 'pinyacampoy' ),
        'type'    => 'file',

        'options' => array(
            'url' => false
        ),
        'text'    => array(
            'add_upload_file_text' => esc_html__( 'Upload Background', 'pinyacampoy' ),
        ),
        'query_args' => array(
            'type' => array(
                'image/gif',
                'image/jpeg',
                'image/png'
            )
        ),
        'preview_size' => 'medium'
    ) );

    /* --------------------------------------------------------------
        3.- SUCCESS STORIES
    -------------------------------------------------------------- */
    $cmb_home_success = new_cmb2_box( array(
        'id'            => $prefix . 'home_success_metabox',
        'title'         => esc_html__( 'Home: Success Stories Section', 'pinyacampoy' ),
        'object_types'  => array( 'page' ), // Post type
        'show_on'       => array( 'key' => 'front-page', 'value' => '' ),
        'closed'        => true
    ) );

    $cmb_home_success->add_field( array(
        'name'       => esc_html__( 'Activate Section', 'pinyacampoy' ),
        'desc'       => esc_html__( 'Check if you want to activate this section', 'pinyacampoy' ),
        'id'         => $prefix . 'activate_success',
        'type'       => 'checkbox',
    ) );

    $cmb_home_success->add_field( array(
        'name'       => esc_html__( 'Section title', 'pinyacampoy' ),
        'desc'       => esc_html__( 'Enter the title for this section', 'pinyacampoy' ),
        'id'         => $prefix . 'success_title',
        'type'       => 'text',
    ) );

    $group_field_id = $cmb_home_success->add_field( array(
        'id'          => $prefix . 'home_success_group',
        'type'        => 'group',
        'title'         => esc_html__( 'About Slider', 'pinyacampoy' ),
        'description' => __( 'Slides inside this Slider', 'pinyacampoy' ),
        'options'     => array(
            'group_title'       => __( 'Slide {#}', 'pinyacampoy' ),
            'add_button'        => __( 'Add other Slide', 'pinyacampoy' ),
            'remove_button'     => __( 'Remove Slide', 'pinyacampoy' ),
            'sortable'          => true,
            'closed'         => true,
            'remove_confirm' => esc_html__( 'Are you sure to remove this slide?', 'pinyacampoy' )
        )
    ) );

    $cmb_home_success->add_group_field( $group_field_id, array(
        'id'   => 'title',
        'name'      => esc_html__( 'Slider Title', 'pinyacampoy' ),
        'desc'      => esc_html__( 'Enter a descriptive title for all slides', 'pinyacampoy' ),
        'type' => 'text'
    ) );

    $cmb_home_success->add_group_field( $group_field_id, array(
        'id'   => 'description',
        'name'      => esc_html__( 'Slider Text', 'pinyacampoy' ),
        'desc'      => esc_html__( 'Enter a descriptive text for all slides', 'pinyacampoy' ),
        'type' => 'textarea_small'
    ) );

    $cmb_home_success->add_group_field( $group_field_id, array(
        'id'   => 'bg_image',
        'name'      => esc_html__( 'Slider Image', 'pinyacampoy' ),
        'desc'      => esc_html__( 'Upload a background for this slide', 'pinyacampoy' ),
        'type'    => 'file',

        'options' => array(
            'url' => false
        ),
        'text'    => array(
            'add_upload_file_text' => esc_html__( 'Upload Background', 'pinyacampoy' ),
        ),
        'query_args' => array(
            'type' => array(
                'image/gif',
                'image/jpeg',
                'image/png'
            )
        ),
        'preview_size' => 'medium'
    ) );

    /* --------------------------------------------------------------
        4.- COLLECTION
    -------------------------------------------------------------- */


    $array_cat = get_terms(array('taxonomy' => 'product_cat', 'hide_empty' => false));

    if (!empty($array_cat)) {
        foreach ($array_cat as $item) {
            $array_categories[$item->term_id] = $item->name;
        }
    } else {
        $array_categories = array();
    }


    $cmb_home_collection = new_cmb2_box( array(
        'id'            => $prefix . 'home_collection_metabox',
        'title'         => esc_html__( 'Home: Collection Section', 'pinyacampoy' ),
        'object_types'  => array( 'page' ), // Post type
        'show_on'       => array( 'key' => 'front-page', 'value' => '' ),
        'closed'        => true
    ) );

    $cmb_home_collection->add_field( array(
        'name'       => esc_html__( 'Activate Section', 'pinyacampoy' ),
        'desc'       => esc_html__( 'Check if you want to activate this section', 'pinyacampoy' ),
        'id'         => $prefix . 'activate_collection',
        'type'       => 'checkbox',
    ) );

    $cmb_home_collection->add_field( array(
        'name'       => esc_html__( 'Section title', 'pinyacampoy' ),
        'desc'       => esc_html__( 'Enter the title for this section', 'pinyacampoy' ),
        'id'         => $prefix . 'collection_title',
        'type'       => 'text',
    ) );

    $cmb_home_collection->add_field( array(
        'id'         => $prefix . 'home_collection_cat',
        'name'       => esc_html__( 'Select Collection Category', 'pinyacampoy' ),
        'desc'       => esc_html__( 'Select the product category for this section', 'pinyacampoy' ),
        'type'             => 'select',
        'show_option_none' => true,
        'default'          => 'custom',
        'options'          => $array_categories
    ) );

    $cmb_home_collection->add_field( array(
        'id'         => $prefix . 'home_collection_cat1',
        'name'       => esc_html__( 'Select First Collection Category', 'pinyacampoy' ),
        'desc'       => esc_html__( 'Select the product category for "Men" in this section', 'pinyacampoy' ),
        'type'             => 'select',
        'show_option_none' => true,
        'default'          => 'custom',
        'options'          => $array_categories
    ) );

    $cmb_home_collection->add_field( array(
        'id'         => $prefix . 'home_collection_cat2',
        'name'       => esc_html__( 'Select Second Collection Category', 'pinyacampoy' ),
        'desc'       => esc_html__( 'Select the product category forfor "Women" in  this section', 'pinyacampoy' ),
        'type'             => 'select',
        'show_option_none' => true,
        'default'          => 'custom',
        'options'          => $array_categories
    ) );

    $cmb_home_collection->add_field( array(
        'name'       => esc_html__( 'Show Product Category Info?', 'pinyacampoy' ),
        'desc'       => esc_html__( 'Check if you want to show this information', 'pinyacampoy' ),
        'id'         => $prefix . 'show_category_info',
        'type'       => 'checkbox',
    ) );

    /* --------------------------------------------------------------
        5.-  CUSTOM SHOES
    -------------------------------------------------------------- */
    $cmb_home_customshoes = new_cmb2_box( array(
        'id'            => $prefix . 'home_customshoes_metabox',
        'title'         => esc_html__( 'Home: Custom Shoes Section', 'pinyacampoy' ),
        'object_types'  => array( 'page' ), // Post type
        'show_on'       => array( 'key' => 'front-page', 'value' => '' ),
        'closed'        => true
    ) );

    $cmb_home_customshoes->add_field( array(
        'name'       => esc_html__( 'Activate Section', 'pinyacampoy' ),
        'desc'       => esc_html__( 'Check if you want to activate this section', 'pinyacampoy' ),
        'id'         => $prefix . 'activate_cshoes',
        'type'       => 'checkbox',
    ) );

    $cmb_home_customshoes->add_field( array(
        'name'       => esc_html__( 'Section title', 'pinyacampoy' ),
        'desc'       => esc_html__( 'Enter the title for this section', 'pinyacampoy' ),
        'id'         => $prefix . 'cshoes_title',
        'type'       => 'text',
    ) );

    $group_field_id = $cmb_home_customshoes->add_field( array(
        'id'          => $prefix . 'home_cshoes_group',
        'type'        => 'group',
        'description' => __( 'Slides inside this Slider', 'pinyacampoy' ),
        'options'     => array(
            'group_title'       => __( 'Slide {#}', 'pinyacampoy' ),
            'add_button'        => __( 'Add other Slide', 'pinyacampoy' ),
            'remove_button'     => __( 'Remove Slide', 'pinyacampoy' ),
            'sortable'          => true,
            'closed'         => true,
            'remove_confirm' => esc_html__( 'Are you sure to remove this slide?', 'pinyacampoy' )
        )
    ) );

    $cmb_home_customshoes->add_group_field( $group_field_id, array(
        'id'   => 'title',
        'name'      => esc_html__( 'Slider Title', 'pinyacampoy' ),
        'desc'      => esc_html__( 'Enter a descriptive title for all slides', 'pinyacampoy' ),
        'type' => 'text'
    ) );

    $cmb_home_customshoes->add_group_field( $group_field_id, array(
        'id'   => 'description',
        'name'      => esc_html__( 'Slider Text', 'pinyacampoy' ),
        'desc'      => esc_html__( 'Enter a descriptive text for all slides', 'pinyacampoy' ),
        'type'    => 'wysiwyg',
        'options' => array(
            'textarea_rows' => get_option('default_post_edit_rows', 4),
            'teeny' => false
        )
    ) );

    $cmb_home_customshoes->add_group_field( $group_field_id, array(
        'id'   => 'single_image',
        'name'      => esc_html__( 'Slider Image', 'pinyacampoy' ),
        'desc'      => esc_html__( 'Upload a background for this slide', 'pinyacampoy' ),
        'type'    => 'file',

        'options' => array(
            'url' => false
        ),
        'text'    => array(
            'add_upload_file_text' => esc_html__( 'Upload Background', 'pinyacampoy' ),
        ),
        'query_args' => array(
            'type' => array(
                'image/gif',
                'image/jpeg',
                'image/png'
            )
        ),
        'preview_size' => 'medium'
    ) );

    $cmb_home_customshoes->add_group_field( $group_field_id, array(
        'id'   => 'bg_image',
        'name'      => esc_html__( 'Slider Images', 'pinyacampoy' ),
        'desc'      => esc_html__( 'Upload a background for this slide', 'pinyacampoy' ),
        'type' => 'file_list',
        'preview_size' => array( 100, 100 ),
        'query_args' => array( 'type' => 'image' ),
        'text' => array(
            'add_upload_files_text' => esc_html__( 'Upload Images', 'pinyacampoy' ),
            'remove_image_text' => esc_html__( 'Remove Images', 'pinyacampoy' ),
            'file_text' => esc_html__( 'Images', 'pinyacampoy' ),
            'file_download_text' => esc_html__( 'Download', 'pinyacampoy' ),
            'remove_text' => esc_html__( 'Remove', 'pinyacampoy' ),
        )
    ) );




    /* --------------------------------------------------------------
        5.- HOW IT WORKS
    -------------------------------------------------------------- */
    $cmb_home_works = new_cmb2_box( array(
        'id'            => $prefix . 'home_works_metabox',
        'title'         => esc_html__( 'Home: How it works Section', 'pinyacampoy' ),
        'object_types'  => array( 'page' ), // Post type
        'show_on'       => array( 'key' => 'front-page', 'value' => '' ),
        'closed'        => true
    ) );

    $cmb_home_works->add_field( array(
        'name'       => esc_html__( 'Activate Section', 'pinyacampoy' ),
        'desc'       => esc_html__( 'Check if you want to activate this section', 'pinyacampoy' ),
        'id'         => $prefix . 'activate_works',
        'type'       => 'checkbox',
    ) );

    $cmb_home_works->add_field( array(
        'name'       => esc_html__( 'Section title', 'pinyacampoy' ),
        'desc'       => esc_html__( 'Enter the title for this section', 'pinyacampoy' ),
        'id'         => $prefix . 'works_title',
        'type'       => 'text',
    ) );

    $cmb_home_works->add_field( array(
        'id'            => $prefix . 'home_works_content',
        'name'      => esc_html__( 'Content Text', 'pinyacampoy' ),
        'desc'      => esc_html__( 'Enter a descriptive text for this section', 'pinyacampoy' ),
        'type'    => 'wysiwyg',
        'options' => array(
            'textarea_rows' => get_option('default_post_edit_rows', 4),
            'teeny' => false
        )
    ) );

    $cmb_home_works->add_field( array(
        'id'            => $prefix . 'home_works_image',
        'name'      => esc_html__( 'Background Image', 'pinyacampoy' ),
        'desc'      => esc_html__( 'Upload a background for this slide', 'pinyacampoy' ),
        'type'    => 'file',

        'options' => array(
            'url' => false
        ),
        'text'    => array(
            'add_upload_file_text' => esc_html__( 'Upload Background', 'pinyacampoy' ),
        ),
        'query_args' => array(
            'type' => array(
                'image/gif',
                'image/jpeg',
                'image/png'
            )
        ),
        'preview_size' => 'medium'
    ) );

    /* --------------------------------------------------------------
    6.- THANKS SECTION
    -------------------------------------------------------------- */
    $cmb_landing_thanks = new_cmb2_box( array(
        'id'            => $prefix . 'thanks_metabox',
        'title'         => esc_html__( 'Página de Agradecimiento', 'pinyacampoy' ),
        'object_types'  => array( 'page' ),
        'show_on'      => array( 'key' => 'slug', 'value' => array('gracias', 'thanks') ),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'cmb_styles' => true,
        'closed'     => false
    ) );

    $cmb_landing_thanks->add_field( array(
        'id'      => $prefix . 'main_bg',
        'name'      => esc_html__( 'Imagen de Fondo', 'pinyacampoy' ),
        'desc'      => esc_html__( 'Ingrese un fondo apropiado para el Hero Principal', 'pinyacampoy' ),
        'type'    => 'file',
        'options' => array(
            'url' => false
        ),
        'text'    => array(
            'add_upload_file_text' => esc_html__( 'Cargar Fondo del Hero', 'pinyacampoy' ),
        ),
        'query_args' => array(
            'type' => array(
                'image/gif',
                'image/jpeg',
                'image/png'
            )
        ),
        'preview_size' => 'medium'
    ) );



    /* --------------------------------------------------------------
    7.- THANKS SECTION
    -------------------------------------------------------------- */
    $cmb_product_frame_metabox = new_cmb2_box( array(
        'id'            => $prefix . 'product_metabox',
        'title'         => esc_html__( 'Product: Extra Information', 'pinyacampoy' ),
        'object_types'  => array( 'product' ),
        'context'    => 'side',
        'priority'   => 'high',
        'show_names' => true,
        'cmb_styles' => true,
        'closed'     => false
    ) );

    $cmb_product_frame_metabox->add_field( array(
        'id'      => $prefix . 'frame_bg',
        'name'      => esc_html__( 'Marco del Producto', 'pinyacampoy' ),
        'desc'      => esc_html__( 'Inserte un marco personalizado en PNG para este producto - Si lo deja vacio, se usara uno predeterminado', 'pinyacampoy' ),
        'type'    => 'file',
        'options' => array(
            'url' => false
        ),
        'text'    => array(
            'add_upload_file_text' => esc_html__( 'Cargar Marco del Producto', 'pinyacampoy' ),
        ),
        'query_args' => array(
            'type' => array(
                'image/gif',
                'image/jpeg',
                'image/png'
            )
        ),
        'preview_size' => 'medium'
    ) );

}
