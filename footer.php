<?php $social_options = get_option('pcy_social_settings'); ?>
<footer class="container-fluid p-0" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
    <div class="row no-gutters">
        <div class="the-footer col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fluid">
                <div class="row align-items-start justify-content-between">
                    <div class="footer-item col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="" class="img-fluid" />
                    </div>
                    <div class="footer-item footer-social col-xl-2 col-lg-3 col-md-4 col-sm-5 col-12">
                        <h4><?php _e('Follow Us', 'pinyacampoy'); ?></h4>
                        <div class="social-container">
                            <?php if ($social_options['facebook'] != '') {  ?>
                            <a href="<?php echo $social_options['facebook']; ?>" title="<?php _e('Haz clic aquí para visitar nuestro perfil', 'pinyacampoy'); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
                            <?php } ?>
                            <?php if ($social_options['twitter'] != '') {  ?>
                            <a href="<?php echo $social_options['twitter']; ?>" title="<?php _e('Haz clic aquí para visitar nuestro perfil', 'pinyacampoy'); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
                            <?php } ?>
                            <?php if ($social_options['instagram'] != '') {  ?>
                            <a href="<?php echo $social_options['instagram']; ?>" title="<?php _e('Haz clic aquí para visitar nuestro perfil', 'pinyacampoy'); ?>" target="_blank"><i class="fa fa-instagram"></i></a>
                            <?php } ?>
                            <?php if ($social_options['youtube'] != '') {  ?>
                            <a href="<?php echo $social_options['youtube']; ?>" title="<?php _e('Haz clic aquí para visitar nuestro perfil', 'pinyacampoy'); ?>" target="_blank"><i class="fa fa-youtube-play"></i></a>
                            <?php } ?>
                        </div>

                    </div>
                    <div class="w-100"></div>
                    <div class="footer-copy col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <hr>
                        <h6>Copyright &copy; 2020 - <?php printf( __('Developed by por <a href="%s">SMG | Digital Marketing Agency</a>', 'pinyacampoy'), 'http://screen.com.ve'); ?></h6>
                    </div>

                </div>
            </div>
        </div>
    </div>
</footer>

<!-- APPOINTMENT MODAL -->
<div class="modal modal-custom fade" id="ModalAppointment" tabindex="-1" role="dialog" aria-labelledby="ModalAppointmentLabel" aria-hidden="true">
    <div class="modal-dialog  modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php $modal_options = get_option('pcy_modal_settings'); ?>
                <h2><?php echo $modal_options['modal_title']; ?></h2>
                <?php echo apply_filters('the_content', $modal_options['modal_text']); ?>
                <?php get_template_part('templates/form-container'); ?>
            </div>
        </div>
    </div>
</div>
<!-- PRODUCT MODAL -->
<div class="modal modal-product fade" id="ModalProduct" tabindex="-1" role="dialog" aria-labelledby="ModalProductLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
        <div class="modal-content">

        </div>
    </div>
</div>
<?php $activate_video = get_post_meta(get_the_ID(), 'pcy_activate_video', true); ?>
<?php if ($activate_video == 'on') {  ?>
<!-- VIDEO MODAL -->
<div class="modal modal-video fade" id="ModalVideo" tabindex="-1" role="dialog" aria-labelledby="ModalVideoLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="videoBody" class="embed-responsive embed-responsive-16by9">

                </div>
            </div>
        </div>
    </div>
</div>
<!-- GALLERY MODAL -->
<div class="modal modal-gallery fade" id="modalGallery" tabindex="-1" role="dialog" aria-labelledby="ModalGalleryLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
        <div id="galleryBody" class="modal-content">

        </div>
    </div>
</div>
<?php } ?>
<?php wp_footer(); ?>
</body>

</html>
