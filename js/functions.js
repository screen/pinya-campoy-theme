var passd = true;

function isValidEmailAddress(emailAddress) {
    'use strict';
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
}

var owlSlider = jQuery('.gallery-image-slider').owlCarousel({
    items: 1,
    loop: true,
    nav: false,
    dots: true,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    autoplay: true,
    autoplayHoverPause: true,
    autoplaySpeed: 12000
});

jQuery(window).resize(function (e) {
    owlSlider.trigger('refresh.owl.carousel');
});

jQuery(document).ready(function ($) {
    "use strict";

    jQuery('#sticker').sticky({
        topSpacing: 0,
        zIndex: 99
    });

    jQuery('.slider-content').owlCarousel({
        items: 1,
        loop: true,
        nav: false,
        dots: true,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        autoplay: true,
        autoplayHoverPause: true,
        autoplaySpeed: 12000
    });

    jQuery('.about-slider-content').owlCarousel({
        items: 1,
        loop: true,
        nav: true,
        navText: ['<span class="custom-slider-nav custom-prev"></span>', '<span class="custom-slider-nav custom-next"></span>'],
        dots: false,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        autoplay: true,
        autoplayHoverPause: true,
        autoplaySpeed: 12000,
        responsive: {
            768: {
                nav: true,
            },
            0: {
                nav: false,
            }
        }
    });

    jQuery('.stories-slider-content').owlCarousel({
        items: 1,
        loop: true,
        nav: true,
        navText: ['<span class="custom-slider-nav custom-prev"></span>', '<span class="custom-slider-nav custom-next"></span>'],
        dots: false,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        autoplay: true,
        autoplayHoverPause: true,
        autoplaySpeed: 12000,
        responsive: {
            768: {
                nav: true,
            },
            0: {
                nav: false,
            }
        }
    });

    jQuery('.custom-shoes-slider').owlCarousel({
        items: 1,
        loop: true,
        nav: true,
        navText: ['<span class="custom-slider-nav custom-prev"></span>', '<span class="custom-slider-nav custom-next"></span>'],
        dots: false,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        autoplay: true,
        autoplayHoverPause: true,
        autoplaySpeed: 12000,
        responsive: {
            768: {
                nav: true,
            },
            0: {
                nav: false,
            }
        }
    });

    jQuery('.collection-slider').owlCarousel({
        items: 3,
        margin: 120,
        loop: true,
        nav: true,
        navText: ['<span class="custom-slider-nav custom-prev"></span>', '<span class="custom-slider-nav custom-next"></span>'],
        dots: false,
        autoplay: true,
        autoplayHoverPause: true,
        autoplaySpeed: 5000,
        responsive: {
            1440: {
                margin: 120
            },
            1366: {
                margin: 120
            },
            1280: {
                margin: 120
            },
            1170: {
                margin: 100
            },
            1024: {
                margin: 30
            },
            768: {
                items: 2,
                margin: 30,
                nav: true
            },
            533: {
                margin: 10,
                items: 1,
                nav: false
            },
            0: {
                margin: 10,
                items: 1,
                nav: false
            }
        }
    });

    jQuery('.dropdown-collection-item').on('click', function (e) {
        var nameCollection = jQuery(this).attr('aria-controls');
        jQuery('.selected-collection').html(nameCollection);
        jQuery('.tab-content .tab-pane').each(function () {
            jQuery(this).removeClass('active');
        });
        jQuery('#' + nameCollection).tab('show');
    });


    jQuery('input[name=fullname]').on('change', function () {
        if (jQuery('input[name=fullname]').val() == '') {
            jQuery('input[name=fullname]').next('small').removeClass('d-none').html('error');
        } else {
            if (jQuery('input[name=fullname]').val().length < 3) {

                jQuery('input[name=fullname]').next('small').removeClass('d-none').html(custom_admin_url.invalid_nombre);
            } else {
                jQuery('input[name=fullname]').next('small').addClass('d-none');
            }
        }
    });


    jQuery('input[name=email]').on('change', function () {
        if (jQuery('input[name=email]').val() == '') {

            jQuery('input[name=email]').next('small').removeClass('d-none').html(custom_admin_url.error_email);
        } else {
            if (!isValidEmailAddress(jQuery('input[name=email]').val())) {

                jQuery('input[name=email]').next('small').removeClass('d-none').html(custom_admin_url.invalid_email);
            } else {
                jQuery('input[name=email]').next('small').addClass('d-none');
            }
        }
    });

    jQuery('input[name=phone]').on('change', function () {
        if (jQuery('input[name=phone]').val() == '') {

            jQuery('input[name=phone]').next('small').removeClass('d-none').html(custom_admin_url.error_phone);
        } else {
            if (jQuery('input[name=phone]').val().length < 3) {

                jQuery('input[name=phone]').next('small').removeClass('d-none').html(custom_admin_url.invalid_phone);
            } else {
                jQuery('input[name=phone]').next('small').addClass('d-none');
            }
        }
    });

    jQuery('select[name=pais]').on('change', function () {
        if (jQuery('select[name=pais] :selected').val() == '') {

            jQuery('select[name=pais]').next('small').removeClass('d-none').html(custom_admin_url.error_pais);
        } else {
            jQuery('select[name=pais]').next('small').addClass('d-none');
        }
    });

    jQuery('textarea[name=subject]').on('change', function () {
        if (jQuery('textarea[name=subject]').val() == '') {

            jQuery('textarea[name=subject]').next('small').removeClass('d-none').html(custom_admin_url.error_mensaje);
        } else {
            jQuery('textarea[name=subject]').next('small').addClass('d-none');
        }
    });

    jQuery('form.contact-form-container').on('submit', function (e) {
        "use strict";
        passd = true;
        e.preventDefault();
        if (jQuery('input[name=fullname]').val() == '') {
            passd = false;
            jQuery('input[name=fullname]').next('small').removeClass('d-none').html(custom_admin_url.error_nombre);
        } else {
            if (jQuery('input[name=fullname]').val().length < 3) {
                passd = false;
                jQuery('input[name=fullname]').next('small').removeClass('d-none').html(custom_admin_url.invalid_nombre);
            } else {
                jQuery('input[name=fullname]').next('small').addClass('d-none');
            }
        }

        if (jQuery('input[name=email]').val() == '') {
            passd = false;
            jQuery('input[name=email]').next('small').removeClass('d-none').html(custom_admin_url.error_email);
        } else {
            if (!isValidEmailAddress(jQuery('input[name=email]').val())) {
                passd = false;
                jQuery('input[name=email]').next('small').removeClass('d-none').html(custom_admin_url.invalid_email);
            } else {
                jQuery('input[name=email]').next('small').addClass('d-none');
            }
        }

        if (jQuery('input[name=phone]').val() == '') {
            passd = false;
            jQuery('input[name=phone]').next('small').removeClass('d-none').html(custom_admin_url.error_phone);
        } else {
            if (jQuery('input[name=phone]').val().length < 3) {
                passd = false;
                jQuery('input[name=phone]').next('small').removeClass('d-none').html(custom_admin_url.invalid_phone);
            } else {
                jQuery('input[name=phone]').next('small').addClass('d-none');
            }
        }

        if (jQuery('input[name=subject]').val() == '') {
            passd = false;
            jQuery('input[name=subject]').next('small').removeClass('d-none').html(custom_admin_url.error_mensaje);
        } else {
            jQuery('input[name=subject]').next('small').addClass('d-none');
        }


        if (jQuery('textarea[name=comments]').val() == '') {
            passd = false;
            jQuery('textarea[name=comments]').next('small').removeClass('d-none').html(custom_admin_url.error_mensaje);
        } else {
            jQuery('textarea[name=comments]').next('small').addClass('d-none');
        }

        if (passd == true) {
            jQuery.ajax({
                type: 'POST',
                url: custom_admin_url.ajax_url,
                data: {
                    action: 'send_contact_form',
                    info: jQuery('.contact-form-container').serialize()
                },
                beforeSend: function () {
                    jQuery('.contact-form-loader').append('<div class="lds-ring"><div></div><div></div><div></div><div></div></div>');
                },
                success: function (response) {
                    jQuery('.contact-form-loader').html('');
                    var res = response.data.replace('&amp;nsbp;', '<br/>');
                    if (response.success == true) {
                        window.location.replace(custom_admin_url.thanks_url);
                    } else {
                        jQuery('.contact-form-response').html(res);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                }
            });
        }

    });

    jQuery('form.page-contact-form-container').on('submit', function (e) {
        "use strict";
        passd = true;
        e.preventDefault();
        if (jQuery('.page-contact-form-container input[name=fullname]').val() == '') {
            passd = false;
            jQuery('.page-contact-form-container input[name=fullname]').next('small').removeClass('d-none').html(custom_admin_url.error_nombre);
        } else {
            if (jQuery('.page-contact-form-container input[name=fullname]').val().length < 3) {
                passd = false;
                jQuery('.page-contact-form-container input[name=fullname]').next('small').removeClass('d-none').html(custom_admin_url.invalid_nombre);
            } else {
                jQuery('.page-contact-form-container input[name=fullname]').next('small').addClass('d-none');
            }
        }

        if (jQuery('.page-contact-form-container input[name=email]').val() == '') {
            passd = false;
            jQuery('.page-contact-form-container input[name=email]').next('small').removeClass('d-none').html(custom_admin_url.error_email);
        } else {
            if (!isValidEmailAddress(jQuery('.page-contact-form-container input[name=email]').val())) {
                passd = false;
                jQuery('.page-contact-form-container input[name=email]').next('small').removeClass('d-none').html(custom_admin_url.invalid_email);
            } else {
                jQuery('.page-contact-form-container input[name=email]').next('small').addClass('d-none');
            }
        }

        if (jQuery('.page-contact-form-container input[name=phone]').val() == '') {
            passd = false;
            jQuery('.page-contact-form-container input[name=phone]').next('small').removeClass('d-none').html(custom_admin_url.error_phone);
        } else {
            if (jQuery('.page-contact-form-container input[name=phone]').val().length < 3) {
                passd = false;
                jQuery('.page-contact-form-container input[name=phone]').next('small').removeClass('d-none').html(custom_admin_url.invalid_phone);
            } else {
                jQuery('.page-contact-form-container input[name=phone]').next('small').addClass('d-none');
            }
        }

        if (jQuery('.page-contact-form-container input[name=subject]').val() == '') {
            passd = false;
            jQuery('.page-contact-form-container input[name=subject]').next('small').removeClass('d-none').html(custom_admin_url.error_mensaje);
        } else {
            jQuery('.page-contact-form-container input[name=subject]').next('small').addClass('d-none');
        }


        if (jQuery('.page-contact-form-container textarea[name=comments]').val() == '') {
            passd = false;
            jQuery('.page-contact-form-container textarea[name=comments]').next('small').removeClass('d-none').html(custom_admin_url.error_mensaje);
        } else {
            jQuery('.page-contact-form-container textarea[name=comments]').next('small').addClass('d-none');
        }

        if (passd == true) {
            jQuery.ajax({
                type: 'POST',
                url: custom_admin_url.ajax_url,
                data: {
                    action: 'send_contact_form',
                    info: jQuery('.page-contact-form-container').serialize()
                },
                beforeSend: function () {
                    jQuery('.page-contact-form-container .contact-form-loader').append('<div class="lds-ring"><div></div><div></div><div></div><div></div></div>');
                },
                success: function (response) {
                    jQuery('.page-contact-form-container .contact-form-loader').html('');
                    var res = response.data.replace('&amp;nsbp;', '<br/>');
                    if (response.success == true) {
                        window.location.replace(custom_admin_url.thanks_url);
                    } else {
                        jQuery('.page-contact-form-container .contact-form-response').html(res);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                }
            });
        }

    });

    jQuery('.collection-item').on('click', function (e) {
        e.preventDefault();
        jQuery.ajax({
            type: 'POST',
            url: custom_admin_url.ajax_url,
            data: {
                action: 'ajax_product_modal',
                product_id: jQuery(this).attr('id')
            },
            beforeSend: function () {
                jQuery('#ModalProduct').modal('show');
                jQuery('#ModalProduct').html('<div class="modal-loader"><div class="lds-ring"><div></div><div></div><div></div><div></div></div></div>');
            },
            success: function (response) {
                jQuery('#ModalProduct').html(response);
                var owlSlider = jQuery('.gallery-image-slider').owlCarousel({
                    items: 1,
                    loop: true,
                    nav: false,
                    dots: true,
                    animateOut: 'fadeOut',
                    animateIn: 'fadeIn',
                    autoplay: true,
                    autoplayHoverPause: true,
                    autoplaySpeed: 3000
                });
                owlSlider.trigger('refresh.owl.carousel');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    });

    jQuery('.navbar-button').on('click', function (e) {
        e.preventDefault();
        jQuery('.navbar-mobile-container').toggleClass('navbar-mobile-container-hidden');
        jQuery(this).toggleClass('navbar-button-opened');
    });

    jQuery('.btn-slider-modal').on('click', function (e) {
        e.preventDefault();
        jQuery('#videoBody').html('');
        var videoHTML = '<video id="videoPage" class="embed-responsive-item" controls preload="none" poster="' + jQuery(this).data('poster') + '"><source src="' + jQuery(this).data('video_webm') + '" type="video/web" /><source src="' + jQuery(this).data('video_mp4') + '" type="video/webm" /><source src="' + jQuery(this).data('video_ogv') + '" type="video/webm" />Video tag not supported. Download the video <a href="' + jQuery(this).data('video_mp4') + '">here</a>.</video>';
        jQuery('#videoBody').html(videoHTML);
        jQuery('#videoPage').get(0).play()
        jQuery('#ModalVideo').modal('toggle');
    });

    jQuery('#ModalVideo').on('shown.bs.modal', function (e) {
        jQuery('#videoPage').get(0).play()
    });

    jQuery('#ModalVideo').on('hide.bs.modal', function (e) {
        jQuery('#videoPage').get(0).pause()
    });


    jQuery('.btn-custom-shoes').on('click', function (e) {
        e.preventDefault();
        jQuery.ajax({
            type: 'POST',
            url: custom_admin_url.ajax_url,
            data: {
                action: 'ajax_modal_gallery',
                gallery_ids: jQuery(this).data('gallery_ids')
            },
            beforeSend: function () {
                jQuery('#modalGallery').modal('show');
                jQuery('#modalGallery').html('<div class="modal-loader"><div class="lds-ring"><div></div><div></div><div></div><div></div></div></div>');
            },
            success: function (response) {
                jQuery('#modalGallery').html(response);
                var owlSlider2 = jQuery('.modal-gallery-image-slider').owlCarousel({
                    items: 1,
                    loop: true,
                    nav: false,
                    dots: true,
                    animateOut: 'fadeOut',
                    animateIn: 'fadeIn',
                    autoplay: true,
                    autoplayHoverPause: true,
                    autoplaySpeed: 3000
                });
                owlSlider2.trigger('refresh.owl.carousel');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    });

});
