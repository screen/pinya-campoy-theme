<?php

/* --------------------------------------------------------------
    ENQUEUE AND REGISTER CSS
-------------------------------------------------------------- */

require_once('includes/wp_enqueue_styles.php');

/* --------------------------------------------------------------
    ENQUEUE AND REGISTER JS
-------------------------------------------------------------- */

if (!is_admin()) add_action('wp_enqueue_scripts', 'pinyacampoy_jquery_enqueue');
function pinyacampoy_jquery_enqueue() {
    wp_deregister_script('jquery');
    wp_deregister_script('jquery-migrate');
    if ($_SERVER['REMOTE_ADDR'] == '::1') {
        /*- JQUERY ON LOCAL  -*/
        wp_register_script( 'jquery', get_template_directory_uri() . '/js/jquery.min.js', false, '3.4.1', false);
        /*- JQUERY MIGRATE ON LOCAL  -*/
        wp_register_script( 'jquery-migrate', get_template_directory_uri() . '/js/jquery-migrate.min.js',  array('jquery'), '3.1.0', false);
    } else {
        /*- JQUERY ON WEB  -*/
        wp_register_script( 'jquery', 'https://code.jquery.com/jquery-3.4.1.min.js', false, '3.4.1', false);
        /*- JQUERY MIGRATE ON WEB  -*/
        wp_register_script( 'jquery-migrate', 'https://code.jquery.com/jquery-migrate-3.1.0.min.js', array('jquery'), '3.1.0', true);
    }
    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-migrate');
}

/* NOW ALL THE JS FILES */

require_once('includes/wp_enqueue_scripts.php');

/* --------------------------------------------------------------
    ADD CUSTOM WALKER BOOTSTRAP
-------------------------------------------------------------- */

add_action( 'after_setup_theme', 'pinyacampoy_register_navwalker' );
function pinyacampoy_register_navwalker(){
    require_once('includes/class-wp-bootstrap-navwalker.php');
}

/* --------------------------------------------------------------
    ADD CUSTOM WORDPRESS FUNCTIONS
-------------------------------------------------------------- */

require_once('includes/wp_custom_functions.php');

/* --------------------------------------------------------------
    ADD REQUIRED WORDPRESS PLUGINS
-------------------------------------------------------------- */

require_once('includes/class-tgm-plugin-activation.php');
require_once('includes/class-required-plugins.php');

/* --------------------------------------------------------------
    ADD CUSTOM WOOCOMMERCE OVERRIDES
-------------------------------------------------------------- */
if ( class_exists( 'WooCommerce' ) ) {
    require_once('includes/wp_woocommerce_functions.php');
}

/* --------------------------------------------------------------
    ADD JETPACK COMPATIBILITY
-------------------------------------------------------------- */
if ( defined( 'JETPACK__VERSION' ) ) {
    require_once('includes/wp_jetpack_functions.php');
}

/* --------------------------------------------------------------
    ADD THEME SUPPORT
-------------------------------------------------------------- */

load_theme_textdomain( 'pinyacampoy', get_template_directory() . '/languages' );
add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio' ));
add_theme_support( 'post-thumbnails' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'title-tag' );
add_theme_support( 'menus' );
add_theme_support( 'customize-selective-refresh-widgets' );
add_theme_support( 'custom-background',
                  array(
                      'default-image' => '',    // background image default
                      'default-color' => 'ffffff',    // background color default (dont add the #)
                      'wp-head-callback' => '_custom_background_cb',
                      'admin-head-callback' => '',
                      'admin-preview-callback' => ''
                  )
                 );
add_theme_support( 'custom-logo', array(
    'height'      => 250,
    'width'       => 250,
    'flex-width'  => true,
    'flex-height' => true,
) );


add_theme_support( 'html5', array(
    'search-form',
    'comment-form',
    'comment-list',
    'gallery',
    'caption',
) );

/* --------------------------------------------------------------
    ADD NAV MENUS LOCATIONS
-------------------------------------------------------------- */

register_nav_menus( array(
    'header_menu' => __( 'Menu Header - Principal', 'pinyacampoy' )
) );

/* --------------------------------------------------------------
    ADD DYNAMIC SIDEBAR SUPPORT
-------------------------------------------------------------- */

add_action( 'widgets_init', 'pinyacampoy_widgets_init' );
function pinyacampoy_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Sidebar Principal', 'pinyacampoy' ),
        'id' => 'main_sidebar',
        'description' => __( 'Estos widgets seran vistos en las entradas y páginas del sitio', 'pinyacampoy' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );

    register_sidebars( 4, array(
        'name'          => __('Footer Section %d', 'pinyacampoy'),
        'id'            => 'sidebar_footer',
        'description'   => __('Footer Section', 'pinyacampoy'),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>'
    ) );

    //    register_sidebar( array(
    //        'name' => __( 'Shop Sidebar', 'pinyacampoy' ),
    //        'id' => 'shop_sidebar',
    //        'description' => __( 'Estos widgets seran vistos en Tienda y Categorias de Producto', 'pinyacampoy' ),
    //        'before_widget' => '<li id='%1$s' class='widget %2$s'>',
    //        'after_widget'  => '</li>',
    //        'before_title'  => '<h2 class='widgettitle'>',
    //        'after_title'   => '</h2>',
    //    ) );
}

/* --------------------------------------------------------------
    CUSTOM ADMIN LOGIN
-------------------------------------------------------------- */

function custom_admin_styles() {
    $version_remove = NULL;
    wp_register_style('wp-admin-style', get_template_directory_uri() . '/css/custom-wordpress-admin-style.css', false, $version_remove, 'all');
    wp_enqueue_style('wp-admin-style');
}
add_action('login_head', 'custom_admin_styles');
add_action('admin_init', 'custom_admin_styles');


function dashboard_footer() {
    echo '<span id="footer-thankyou">';
    _e ('Gracias por crear con ', 'pinyacampoy' );
    echo '<a href="http://wordpress.org/" target="_blank">WordPress.</a> - ';
    _e ('Tema desarrollado por ', 'pinyacampoy' );
    echo '<a href="http://www.screenmediagroup.com/" target="_blank">SMG</a></span>';
}
add_filter('admin_footer_text', 'dashboard_footer');

/* --------------------------------------------------------------
    ADD CUSTOM METABOX
-------------------------------------------------------------- */

require_once('includes/wp_custom_metabox.php');

/* --------------------------------------------------------------
    ADD CUSTOM POST TYPE
-------------------------------------------------------------- */

//require_once('includes/wp_custom_post_type.php');

/* --------------------------------------------------------------
    ADD CUSTOM THEME CONTROLS
-------------------------------------------------------------- */

require_once('includes/wp_custom_theme_control.php');

/* --------------------------------------------------------------
    ADD CUSTOM IMAGE SIZE
-------------------------------------------------------------- */
if ( function_exists('add_theme_support') ) {
    add_theme_support('post-thumbnails');
    set_post_thumbnail_size( 9999, 400, true);
}
if ( function_exists('add_image_size') ) {
    add_image_size('avatar', 100, 100, true);
    add_image_size('slider_img', 1500, 1000, array('center', 'center'));
    add_image_size('about_img', 950, 850, array('center', 'center'));
    add_image_size('stories_img', 970, 780, array('center', 'center'));
    add_image_size('product_img', 480, 480, array('center', 'center'));
}

/* --------------------------------------------------------------
    ADD CUSTOM AJAX HANDLER
-------------------------------------------------------------- */



add_action('wp_ajax_ajax_modal_gallery', 'ajax_modal_gallery_handler');
add_action('wp_ajax_nopriv_ajax_modal_gallery', 'ajax_modal_gallery_handler');

function ajax_modal_gallery_handler() {
    $gallery_ids = $_POST['gallery_ids'];
    $gallery_array = explode(',', $gallery_ids);
    ob_start();
?>
<div class="modal-dialog modal-xl modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="modal-gallery-image-slider owl-carousel owl-theme">
                            <?php foreach ($gallery_array as $gallery_item) { ?>
                            <div class="item">
                                <?php echo wp_get_attachment_image($gallery_item, 'full', array('class' => 'img-fluid img-product-modal')); ?>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    $content = ob_get_clean();
    echo $content;
    wp_die();
}

add_action('wp_ajax_ajax_product_modal', 'ajax_product_modal_handler');
add_action('wp_ajax_nopriv_ajax_product_modal', 'ajax_product_modal_handler');

function ajax_product_modal_handler() {
    $product_id = $_POST['product_id'];
    $post = get_post($product_id);
    $header_options = get_option('pcy_header_settings');
    ob_start();
?>
<div class="modal-dialog modal-xl modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div class="gallery-image-slider owl-carousel owl-theme">
                            <div class="item">
                                <?php echo get_the_post_thumbnail( $post->ID, 'full', array('class' => 'img-fluid img-product-modal'));  ?>
                            </div>
                            <?php $gallery_image = get_post_meta($post->ID, '_product_image_gallery', true); ?>
                            <?php $gallery_image = explode(',', $gallery_image); ?>
                            <?php foreach ($gallery_image as $gallery_item) { ?>
                            <div class="item">
                                <?php echo wp_get_attachment_image($gallery_item, 'full', array('class' => 'img-fluid img-product-modal')); ?>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <h2><?php echo $post->post_title; ?></h2>
                        <?php echo apply_filters('the_content', $post->post_content); ?>
                        <?php $phone_number = $header_options['phone_number']; ?>
                        <?php $message_translated = __("Hi! I'm interested in this shoe model ", 'pinyacampoy'); ?>
                        <?php $message = urlencode($message_translated . '*' . $post->post_title . '*'); ?>
                        <a href="https://api.whatsapp.com/send?phone=<?php echo $phone_number; ?>&text=<?php echo $message; ?>" title="<?php _e('Click here to adquire this product', 'pinyacampoy'); ?>" target="_blank" class="btn btn-md btn-product-modal"><?php _e('I wish it!', 'pinyacampoy'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    $content = ob_get_clean();

    echo $content;
    wp_die();
}

/* --------------------------------------------------------------
    AJAX SEND QUOTE EMAIL
-------------------------------------------------------------- */
add_action('wp_ajax_nopriv_send_contact_form', 'send_contact_form_handler');
add_action('wp_ajax_send_contact_form', 'send_contact_form_handler');

function send_contact_form_handler() {
    if ( defined( 'WP_DEBUG' ) && WP_DEBUG ) {
        error_reporting( E_ALL );
        ini_set( 'display_errors', 1 );
    }

    $header_options = get_option('pcy_header_settings');
    $email_address = $header_options['email_address'];

    parse_str($_POST['info'], $submit);

    //    $google_options = get_option('ioa_google_settings');
    /*
    if ($submit["g-recaptcha-response"]) {
        $post_data = http_build_query(
            array(
                'secret' => $google_options['google_secret'],
                'response' => $submit['g-recaptcha-response'],
                'remoteip' => $_SERVER['REMOTE_ADDR']
            ), '', '&');

        $opts = array('http' =>
                      array(
                          'method'  => 'POST',
                          'header'  => 'Content-type: application/x-www-form-urlencoded',
                          'content' => $post_data
                      )
                     );

        $context  = stream_context_create($opts);
        $response = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
        $captcha_response = json_decode($response);
    }
    if($captcha_response->success == true) {
*/
    ob_start();
    require_once get_theme_file_path( '/includes/contact-email.php' );
    $body = ob_get_clean();
    $body = str_replace( [
        '{fullname}',
        '{email}',
        '{phone}',
        '{subject}',
        '{comments}'
    ], [
        $submit['fullname'],
        $submit['email'],
        $submit['phone'],
        $submit['subject'],
        $submit['comments']
    ], $body );

    $path = ABSPATH . WPINC . '/class-phpmailer.php';

    if (file_exists($path)) {
        require_once($path);
        $mail = new PHPMailer;
    } else {
        require_once ABSPATH . WPINC . '/PHPMailer/PHPMailer.php';
        $mail = new PHPMailer\PHPMailer\PHPMailer;
    }

    $mail->isHTML( true );
    $mail->Body = $body;
    $mail->CharSet = 'UTF-8';
    $mail->addAddress($email_address);
    //$mail->addAddress( 'ochoa.robert1@gmail.com' );
    $mail->setFrom( "noreply@{$_SERVER['SERVER_NAME']}", esc_html( get_bloginfo( 'name' ) ) );
    $mail->Subject = esc_html__( 'Pinya Campoy: New Web Contact', 'pinyacampoy' );

    if ( ! $mail->send() ) {
        wp_send_json_error( esc_html__( 'Your message could not be sent. &nsbp; Please try again.', 'pinyacampoy' ), 400 );
    } else {
        wp_send_json_success( esc_html__( "Thank you for submiting &nsbp; We will be contacting you in the next 24 hours.", 'pinyacampoy' ), 200 );
    }
    /*
    }
*/
    wp_die();
}
