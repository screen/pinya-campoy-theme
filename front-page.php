<?php get_header(); ?>
<?php the_post(); ?>

<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <?php /* MAIN HOME SLIDER SECTION */ ?>
        <?php $activate_slider = get_post_meta(get_the_ID(), 'pcy_activate_slider', true); ?>
        <?php if ($activate_slider == 'on') : ?>
        <section class="the-slider col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fluid">
                <div class="row ">
                    <div class="slider-container col-xl-10 offset-xl-1 col-lg-10 offset-lg-1 col-md-12 col-sm-12 col-12">
                        <?php $home_slider_group = get_post_meta(get_the_ID(), 'pcy_home_slider_group', true); ?>
                        <?php if (!empty($home_slider_group)) : ?>
                        <div class="slider-content owl-carousel owl-theme">
                            <?php foreach ($home_slider_group as $test_item) { ?>
                            <div class="slider-item">
                                <?php echo wp_get_attachment_image( $test_item['bg_image_id'], 'slider_img', false, array('class' => 'img-fluid img-back') ); ?>
                                <div class="slider-item-wrapper">
                                    <div class="slider-item-title">
                                        <h2><?php echo $test_item['title']; ?></h2>
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/vector.png" alt="" />
                                    </div>
                                    <div class="slider-item-content">
                                        <p><?php echo $test_item['description']; ?></p>
                                        <?php if ($test_item['button_title'] != '') { ?>
                                        <?php $style[] = 'color: ' . $test_item['button_txcolor'] . ' !important;'; ?>
                                        <?php $style[] = 'background: ' . $test_item['button_bgcolor'] . ' !important;'; ?>
                                        <?php $style[] = 'border-color: ' . $test_item['button_bgcolor'] . ' !important;'; ?>


                                        <?php if ($test_item['modal'] == 'on') { ?>
                                        <a href="#" style="<?php echo join(' ', $style); ?>" data-video_mp4="<?php echo $test_item['video_mp4']; ?>" data-video_webm="<?php echo $test_item['video_webm']; ?>" data-video_ogv="<?php echo $test_item['video_ogv']; ?>" data-poster="<?php echo $test_item['poster']; ?>" class="btn btn-md btn-slider btn-slider-modal" title="<?php _e('Click here to Book an Appointment', 'pinyacampoy'); ?>"><?php echo $test_item['button_title']; ?></a>
                                        <?php } else { ?>
                                        <a href="<?php echo $test_item['button_url']; ?>" class="btn btn-md btn-slider" title="<?php _e('Click here to Book an Appointment', 'pinyacampoy'); ?>"><?php echo $test_item['button_title']; ?></a>
                                        <?php } ?>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </section>
        <?php endif; ?>

        <?php /* ABOUT US SECTION */ ?>
        <?php $activate_about = get_post_meta(get_the_ID(), 'pcy_activate_about', true); ?>
        <?php if ($activate_about == 'on') : ?>
        <section id="about" class="the-about col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fluid p-0">
                <div class="row no-gutters">
                    <div class="about-slider-container col-12">
                        <?php $home_about_group = get_post_meta(get_the_ID(), 'pcy_home_about_group', true); ?>
                        <?php if (!empty($home_about_group)) : ?>
                        <div class="about-slider-content owl-carousel owl-theme">
                            <?php foreach ($home_about_group as $test_item) { ?>
                            <div class="about-slider-item">
                                <div class="container-fluid p-0">
                                    <div class="row row-about no-gutters">
                                        <div class="slider-item-title d-xl-none d-lg-none d-md-block d-sm-block d-block">
                                            <h2><?php echo $test_item['title']; ?></h2>
                                        </div>
                                        <div class="slider-item-subtitle d-xl-none d-lg-none d-md-block d-sm-block d-block">
                                            <h3><?php echo $test_item['subtitle']; ?></h3>
                                        </div>
                                        <div class="mobile-about-slider-image d-xl-none d-lg-none d-md-block d-sm-block d-block">
                                            <picture class="about-slider-image-content">
                                                <?php echo wp_get_attachment_image( $test_item['bg_image_id'], 'about_img', false, array('class' => 'img-fluid img-slider') ); ?>
                                            </picture>
                                        </div>

                                        <div class="about-slider-item-wrapper col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                            <div class="slider-item-title d-xl-block d-lg-block d-md-none d-sm-none d-none">
                                                <h2><?php echo $test_item['title']; ?></h2>
                                            </div>
                                            <div class="slider-item-subtitle d-xl-block d-lg-block d-md-none d-sm-none d-none">
                                                <h3><?php echo $test_item['subtitle']; ?></h3>
                                            </div>
                                            <div class="slider-item-content">
                                                <p><?php echo apply_filters('the_content', $test_item['description']); ?></p>
                                                <?php $logos_group = $test_item['logo_list']; ?>
                                                <?php if (!empty($logos_group)) { ?>
                                                <div class="slider-item-logos">
                                                    <?php $logos_group = $test_item['logo_list']; ?>
                                                    <?php foreach ($logos_group as $logo_item) { ?>
                                                    <div class="logo-item">
                                                        <img src="<?php echo $logo_item; ?>" alt="<?php _e('Logo', 'pinyacampoy'); ?>" class="img-fluid" />
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="about-slider-image col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 d-xl-block d-lg-block d-md-none d-sm-none d-none">
                                            <picture class="about-slider-image-content">
                                                <?php echo wp_get_attachment_image( $test_item['bg_image_id'], 'about_img', false, array('class' => 'img-fluid img-slider') ); ?>
                                            </picture>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </section>
        <?php endif; ?>

        <?php /* SUCCESS STORIES SECTION */ ?>
        <?php $activate_stories = get_post_meta(get_the_ID(), 'pcy_activate_success', true); ?>
        <?php if ($activate_stories == 'on') : ?>
        <section class="the-stories col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fluid p-0">
                <div class="row no-gutters">
                    <div class="stories-slider-container col-12">
                        <?php $home_stories_group = get_post_meta(get_the_ID(), 'pcy_home_success_group', true); ?>
                        <?php if (!empty($home_stories_group)) : ?>
                        <div class="stories-slider-content owl-carousel owl-theme">
                            <?php foreach ($home_stories_group as $test_item) { ?>
                            <div class="stories-slider-item">
                                <h2><?php echo get_post_meta(get_the_ID(), 'pcy_success_title', true); ?></h2>
                                <div class="stories-slider-image">
                                    <?php echo wp_get_attachment_image( $test_item['bg_image_id'], 'stories_img', false, array('class' => 'img-fluid img-slider') ); ?>
                                </div>
                                <div class="stories-slider-item-wrapper">
                                    <div class="slider-item-title">
                                        <h3><?php echo $test_item['title']; ?></h3>
                                    </div>
                                    <div class="slider-item-content">
                                        <p><?php echo apply_filters('the_content', $test_item['description']); ?></p>
                                    </div>
                                </div>

                            </div>
                            <?php } ?>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </section>
        <?php endif; ?>


        <?php /* COLLECTIONS SECTION */ ?>
        <?php $activate_collections = get_post_meta(get_the_ID(), 'pcy_activate_collection', true); ?>
        <?php if ($activate_collections == 'on') : ?>
        <section id="collection" class="the-collection col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fluid p-0">
                <div class="row no-gutters">
                    <div class="collection-container col-xl-10 offset-xl-1 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="collection-title col-xl-7 col-lg-6 col-md-12 col-sm-12 col-12">
                                    <h2><?php echo get_post_meta(get_the_ID(), 'pcy_collection_title', true); ?></h2>
                                </div>
                                <?php $home_category = get_post_meta(get_the_ID(), 'pcy_home_collection_cat', true); ?>
                                <?php $activate_info_cat = get_post_meta(get_the_ID(), 'pcy_show_category_info', true); ?>
                                <?php if ($activate_info_cat == 'on') : ?>
                                <div class="collection-content col-xl-5 col-lg-6 col-md-12 col-sm-12 col-12">
                                    <?php $category_array = get_term_by('id', $home_category, 'product_cat'); ?>
                                    <?php $name_explode = explode('/', $category_array->name); ?>
                                    <h2>
                                        <?php foreach ($name_explode as $item) { ?>
                                        <span><?php echo $item; ?></span>
                                        <?php } ?>
                                    </h2>
                                    <?php echo apply_filters('the_content', $category_array->description); ?>
                                </div>
                                <?php endif; ?>
                            </div>
                            <div class="row justify-content-center">
                                <div class="collection-slider-selectors col-xl-10 col-lg-10 col-md-12 col-sm-12 col-12 d-xl-block d-lg-block d-md-block d-sm-none d-none">
                                    <div class="collection-slider-selector">
                                        <?php $category_buttons = array(); ?>
                                        <?php $category_buttons[0] = get_post_meta(get_the_ID(), 'pcy_home_collection_cat1', true); ?>
                                        <?php $category_buttons[1] = get_post_meta(get_the_ID(), 'pcy_home_collection_cat2', true); ?>
                                        <?php $i = 1; ?>
                                        <ul class="nav justify-content-center" id="myTab" role="tablist">
                                            <?php foreach ($category_buttons as $button) { ?>
                                            <li class="nav-item">
                                                <?php if ($i == 1) { $class = 'active'; $active_item = $item->name; } else { $class = ''; } ?>
                                                <?php $item = get_term_by('id', $button, 'product_cat'); ?>
                                                <a class="nav-link <?php echo $class; ?>" id="<?php echo $item->slug; ?>-tab" data-toggle="tab" href="#<?php echo $item->slug; ?>" role="tab" aria-controls="<?php echo $item->slug; ?>" aria-selected="false"><?php echo $item->name; ?></a>
                                            </li>
                                            <?php $i++; } ?>
                                        </ul>



                                    </div>
                                </div>
                                <div class="nav-collection-mobile col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-xl-none d-lg-none d-md-none d-sm-block d-block">
                                    <div class="dropdown dropdown-collection">
                                        <button class="btn btn-secondary btn-collection dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <?php $i = 1; ?>
                                            <?php foreach ($category_buttons as $button) { ?>
                                            <?php if ($i == 1) { ?>
                                            <?php $item = get_term_by('id', $button, 'product_cat'); ?>
                                            <span class="selected-collection"><?php echo $item->name; ?></span>
                                            <?php } ?>
                                            <?php $i++; } ?>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <?php $i = 1; ?>
                                            <?php foreach ($category_buttons as $button) { ?>
                                            <?php $item = get_term_by('id', $button, 'product_cat'); ?>
                                            <a class="dropdown-item dropdown-collection-item" id="<?php echo $item->slug; ?>-tab" aria-controls="<?php echo $item->slug; ?>"><?php echo $item->name; ?></a>
                                            <?php } ?>
                                        </div>
                                    </div>

                                </div>
                                <div class="collection-slider-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="tab-content">
                                        <?php $i = 1; ?>
                                        <?php foreach ($category_buttons as $button) { ?>
                                        <?php $item = get_term_by('id', $button, 'product_cat'); ?>
                                        <?php if ($i == 1) { $class = 'active'; } else { $class = ''; } ?>
                                        <div class="tab-pane <?php echo $class; ?>" id="<?php echo $item->slug; ?>" role="tabpanel" aria-labelledby="<?php echo $item->slug; ?>-tab">
                                            <?php $array_products = new WP_Query(array('post_type' => 'product', 'posts_per_page' => 6, 'orderby' => 'date', 'order' => 'DESC', 'tax_query' => array(array('taxonomy' => 'product_cat', 'field' => 'term_id', 'terms' => $item)))); ?>
                                            <?php if ($array_products->have_posts()) : ?>
                                            <div class="collection-slider owl-carousel owl-theme">
                                                <?php while ($array_products->have_posts()) : $array_products->the_post(); ?>
                                                <div id="<?php echo get_the_ID(); ?>" class="collection-item">
                                                    <div class="collection-item-wrapper">
                                                        <?php $frame_bg = get_post_meta(get_the_ID(), 'pcy_frame_bg', true); ?>
                                                        <?php if ($frame_bg == '') { ?>
                                                        <img src="<?php echo get_template_directory_uri(); ?>/images/frame-rectangle.png" alt="" class="img-fluid img-rectangle" />
                                                        <?php } else { ?>
                                                        <img src="<?php echo $frame_bg; ?>" alt="" class="img-fluid img-rectangle" />
                                                        <?php } ?>
                                                        <?php the_post_thumbnail('product_img', array('class' => 'img-fluid img-collection')); ?>
                                                    </div>
                                                </div>
                                                <?php endwhile; ?>
                                            </div>
                                            <?php endif; ?>
                                            <?php wp_reset_query(); ?>
                                        </div>
                                        <?php $i++; } ?>
                                    </div>
                                </div>
                                <div class="collection-catalog-button col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <?php $shop_page_url = get_permalink( woocommerce_get_page_id( 'shop' ) ); ?>
                                    <a href="<?php echo $shop_page_url; ?>" title="<?php _e('View More', 'pinyacampoy'); ?>" class="btn btn-md btn-catalog-more"><?php _e('View More', 'pinyacampoy'); ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php endif; ?>

        <?php /* CUSTOM SHOES SECTION */ ?>
        <?php $activate_cshoes = get_post_meta(get_the_ID(), 'pcy_activate_cshoes', true); ?>
        <?php if ($activate_cshoes == 'on') : ?>
        <section id="customshoes" class="the-custom-shoes col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <h2 class="section-title"><span><?php echo get_post_meta(get_the_ID(), 'pcy_cshoes_title', true); ?></span></h2>
            <?php $home_custom_group = get_post_meta(get_the_ID(), 'pcy_home_cshoes_group', true); ?>
            <?php if (!empty($home_custom_group)) : ?>
            <div class="custom-shoes-slider owl-carousel owl-theme">
                <?php foreach ($home_custom_group as $test_item) { ?>
                <div class="custom-shows-slide-item">
                    <div class="container-fluid p-0">
                        <div class="row no-gutters">
                            <div class="custom-shoes-big-image col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <?php echo wp_get_attachment_image( $test_item['single_image_id'], 'about_img', false, array('class' => 'img-fluid img-slider') ); ?>
                            </div>
                            <div class="custom-shoes-content col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <div class="slider-item-title">
                                    <h3><?php echo $test_item['title']; ?></h3>
                                </div>
                                <div class="slider-item-content">
                                    <p><?php echo apply_filters('the_content', $test_item['description']); ?></p>
                                    <?php if (!empty($test_item['bg_image'])) { ?>
                                    <?php $array_gallery = array(); ?>
                                    <?php foreach ($test_item['bg_image'] as $attach_id => $attach_data) { ?>
                                    <?php $array_gallery[] = $attach_id; ?>
                                    <?php } ?>
                                    <a href="#" data-gallery_ids="<?php echo join(',', $array_gallery); ?>" class="btn btn-md btn-custom-shoes" title="<?php _e('Click here to See Gallery', 'pinyacampoy'); ?>"><?php _e('View Gallery', 'pinyacampoy'); ?></a>
                                    <?php } ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
            <?php endif; ?>
        </section>
        <?php endif; ?>

        <?php /* CUSTOM SHOES SECTION */ ?>
        <?php $activate_works = get_post_meta(get_the_ID(), 'pcy_activate_works', true); ?>
        <?php if ($activate_works == 'on') : ?>
        <section id="works" class="the-works col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fluid p-0">
                <div class="row no-gutters">
                    <div class="works-image col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 d-xl-none d-lg-none d-md-block d-sm-block d-block">
                        <?php $bg_image = get_post_meta(get_the_ID(), 'pcy_home_works_image', true); ?>
                        <img src="<?php echo $bg_image; ?>" alt="" class="img-fluid" />
                    </div>
                    <div class="works-container col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div class="works-container-wrapper">
                            <h2 class="section-title"><span><?php echo get_post_meta(get_the_ID(), 'pcy_works_title', true); ?></span></h2>
                            <?php echo apply_filters('the_content',  get_post_meta(get_the_ID(), 'pcy_home_works_content', true)); ?>
                            <button class="btn btn-md btn-appointment" title="<?php _e('Click here to Book an Appointment', 'pinyacampoy'); ?>" data-toggle="modal" data-target="#ModalAppointment"><?php _e('Book Appointment', 'pinyacampoy'); ?></button>
                        </div>
                    </div>
                    <div class="works-image col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 d-xl-block d-lg-block d-md-none d-sm-none d-none">
                        <?php $bg_image_id = get_post_meta(get_the_ID(), 'pcy_home_works_image_id', true); ?>
                        <?php echo wp_get_attachment_image( $bg_image_id, 'product_img', false, array('class' => 'img-fluid') ); ?>
                    </div>
                </div>
            </div>
        </section>
        <?php endif; ?>
    </div>
</main>
<?php get_footer(); ?>
