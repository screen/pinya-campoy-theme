<?php $social_options = get_option('pcy_social_settings'); ?>
<footer class="container-fluid p-0" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
    <div class="row no-gutters">
        <div class="the-footer col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 d-xl-block d-lg-block d-md-none d-sm-none d-none">
            <div class="container-fluid">
                <div class="row align-items-start justify-content-between">
                    <div class="footer-copy col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <hr>
                        <h6>Copyright &copy; 2020 - <?php printf( __('Developed by por <a href="%s">SMG | Digital Marketing Agency</a>', 'pinyacampoy'), 'http://screen.com.ve'); ?></h6>
                    </div>

                </div>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer() ?>
</body>

</html>
